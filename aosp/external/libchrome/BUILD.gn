# Copyright 2018 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

import("//common-mk/mojom_bindings_generator.gni")
import("//common-mk/pkg_config.gni")

group("all") {
  deps = [
    ":libchrome",
    ":libchrome-test",
  ]
  if (use.mojo) {
    deps += [ ":libmojo" ]
  }
}

config("libchrome_config") {
  # TODO(hidehiko): Consolidate with build_config.h.
  defines = [
    "OS_CHROMEOS",
    "USE_NSS_CERTS",
    "USE_SYSTEM_LIBEVENT",
    "NO_TCMALLOC",
    "MOJO_EDK_LEGACY_PROTOCOL",
  ]
  if (use.asan) {
    defines += [ "LEAK_SANITIZER" ]
  }

  include_dirs = [ "." ]
  cflags = [
    "-Wno-deprecated-register",
    "-Wno-narrowing",
    "-Wno-unused-local-typedefs",
    "-Xclang-only=-Wno-char-subscripts",
  ]
  # Address sanitizer + coverage builds do not support -z,defs.
  if (!(use.asan || use.coverage)) {
    ldflags = [ "-Wl,-z,defs" ]
  }
}

config("base_core_config") {
  cflags = [
    # Suppressing warning in base/strings/stringprintf.cc.
    "-Wno-format-nonliteral",
    # This is for _exit(1) in base/debug/debugger_posix.cc.
    "-Wno-unreachable-code",
  ]
}

libbase_sublibs = [
  {
    name = "base-core"
    output_name = name + "-${libbase_ver}"
    libs = [ "pthread", "rt", "modp_b64" ]
    pkg_deps = [ "glib-2.0", "libevent" ]
    configs = [ ":base_core_config" ]
    sources = [
      "base/allocator/allocator_extension.cc",
      "base/allocator/allocator_shim.cc",
      "base/allocator/allocator_shim_default_dispatch_to_glibc.cc",
      "base/at_exit.cc",
      "base/base64.cc",
      "base/base64url.cc",
      "base/base_paths.cc",
      "base/base_paths_posix.cc",
      "base/base_switches.cc",
      "base/bind_helpers.cc",
      "base/build_time.cc",
      "base/callback_helpers.cc",
      "base/callback_internal.cc",
      "base/command_line.cc",
      "base/cpu.cc",
      "base/debug/activity_tracker.cc",
      "base/debug/alias.cc",
      "base/debug/debugger.cc",
      "base/debug/debugger_posix.cc",
      "base/debug/dump_without_crashing.cc",
      "base/debug/stack_trace.cc",
      "base/debug/stack_trace_posix.cc",
      "base/debug/task_annotator.cc",
      "base/environment.cc",
      "base/feature_list.cc",
      "base/files/file.cc",
      "base/files/file_descriptor_watcher_posix.cc",
      "base/files/file_enumerator.cc",
      "base/files/file_enumerator_posix.cc",
      "base/files/file_path.cc",
      "base/files/file_path_constants.cc",
      "base/files/file_path_watcher.cc",
      "base/files/file_path_watcher_linux.cc",
      "base/files/file_posix.cc",
      "base/files/file_tracing.cc",
      "base/files/file_util.cc",
      "base/files/file_util_linux.cc",
      "base/files/file_util_posix.cc",
      "base/files/important_file_writer.cc",
      "base/files/memory_mapped_file.cc",
      "base/files/memory_mapped_file_posix.cc",
      "base/files/scoped_file.cc",
      "base/files/scoped_temp_dir.cc",
      "base/guid.cc",
      "base/hash.cc",
      "base/json/json_file_value_serializer.cc",
      "base/json/json_parser.cc",
      "base/json/json_reader.cc",
      "base/json/json_string_value_serializer.cc",
      "base/json/json_value_converter.cc",
      "base/json/json_writer.cc",
      "base/json/string_escape.cc",
      "base/lazy_instance.cc",
      "base/location.cc",
      "base/logging.cc",
      "base/md5.cc",
      "base/memory/aligned_memory.cc",
      "base/memory/ref_counted.cc",
      "base/memory/ref_counted_memory.cc",
      "base/memory/shared_memory_helper.cc",
      "base/memory/shared_memory_posix.cc",
      "base/memory/shared_memory_tracker.cc",
      "base/memory/singleton.cc",
      "base/memory/weak_ptr.cc",
      "base/message_loop/incoming_task_queue.cc",
      "base/message_loop/message_loop.cc",
      "base/message_loop/message_loop_task_runner.cc",
      "base/message_loop/message_pump.cc",
      "base/message_loop/message_pump_default.cc",
      "base/message_loop/message_pump_glib.cc",
      "base/message_loop/message_pump_libevent.cc",
      "base/metrics/bucket_ranges.cc",
      "base/metrics/field_trial.cc",
      "base/metrics/field_trial_param_associator.cc",
      "base/metrics/metrics_hashes.cc",
      "base/metrics/histogram_base.cc",
      "base/metrics/histogram.cc",
      "base/metrics/histogram_samples.cc",
      "base/metrics/histogram_snapshot_manager.cc",
      "base/metrics/persistent_histogram_allocator.cc",
      "base/metrics/persistent_memory_allocator.cc",
      "base/metrics/persistent_sample_map.cc",
      "base/metrics/sample_map.cc",
      "base/metrics/sample_vector.cc",
      "base/metrics/sparse_histogram.cc",
      "base/metrics/statistics_recorder.cc",
      "base/path_service.cc",
      "base/pending_task.cc",
      "base/pickle.cc",
      "base/posix/file_descriptor_shuffle.cc",
      "base/posix/global_descriptors.cc",
      "base/posix/safe_strerror.cc",
      "base/posix/unix_domain_socket_linux.cc",
      "base/process/internal_linux.cc",
      "base/process/kill.cc",
      "base/process/kill_posix.cc",
      "base/process/launch.cc",
      "base/process/launch_posix.cc",
      "base/process/memory.cc",
      "base/process/memory_linux.cc",
      "base/process/process_handle.cc",
      "base/process/process_handle_linux.cc",
      "base/process/process_handle_posix.cc",
      "base/process/process_info_linux.cc",
      "base/process/process_iterator.cc",
      "base/process/process_iterator_linux.cc",
      "base/process/process_metrics.cc",
      "base/process/process_metrics_linux.cc",
      "base/process/process_metrics_posix.cc",
      "base/process/process_posix.cc",
      "base/profiler/scoped_profile.cc",
      "base/profiler/scoped_tracker.cc",
      "base/profiler/tracked_time.cc",
      "base/rand_util.cc",
      "base/rand_util_posix.cc",
      "base/run_loop.cc",
      "base/sequence_checker_impl.cc",
      "base/sequenced_task_runner.cc",
      "base/sequence_token.cc",
      "base/sha1.cc",
      "base/strings/pattern.cc",
      "base/strings/safe_sprintf.cc",
      "base/strings/string16.cc",
      "base/strings/string_number_conversions.cc",
      "base/strings/string_piece.cc",
      "base/strings/stringprintf.cc",
      "base/strings/string_split.cc",
      "base/strings/string_util.cc",
      "base/strings/string_util_constants.cc",
      "base/strings/sys_string_conversions_posix.cc",
      "base/strings/utf_string_conversions.cc",
      "base/strings/utf_string_conversion_utils.cc",
      "base/synchronization/atomic_flag.cc",
      "base/synchronization/condition_variable_posix.cc",
      "base/synchronization/lock.cc",
      "base/synchronization/lock_impl_posix.cc",
      "base/synchronization/read_write_lock_posix.cc",
      "base/synchronization/waitable_event_posix.cc",
      "base/synchronization/waitable_event_watcher_posix.cc",
      "base/sync_socket_posix.cc",
      "base/sys_info.cc",
      "base/sys_info_chromeos.cc",
      "base/sys_info_linux.cc",
      "base/sys_info_posix.cc",
      "base/task_runner.cc",
      "base/task/cancelable_task_tracker.cc",
      "base/task_scheduler/scheduler_lock_impl.cc",
      "base/task_scheduler/scoped_set_task_priority_for_current_thread.cc",
      "base/task_scheduler/sequence.cc",
      "base/task_scheduler/sequence_sort_key.cc",
      "base/task_scheduler/task.cc",
      "base/task_scheduler/task_traits.cc",
      "base/third_party/dynamic_annotations/dynamic_annotations.c",
      "base/third_party/icu/icu_utf.cc",
      "base/third_party/nspr/prtime.cc",
      "base/threading/non_thread_safe_impl.cc",
      "base/threading/platform_thread_internal_posix.cc",
      "base/threading/platform_thread_linux.cc",
      "base/threading/platform_thread_posix.cc",
      "base/threading/post_task_and_reply_impl.cc",
      "base/threading/sequenced_task_runner_handle.cc",
      "base/threading/sequenced_worker_pool.cc",
      "base/threading/simple_thread.cc",
      "base/threading/thread.cc",
      "base/threading/thread_checker_impl.cc",
      "base/threading/thread_collision_warner.cc",
      "base/threading/thread_id_name_manager.cc",
      "base/threading/thread_local_storage.cc",
      "base/threading/thread_local_storage_posix.cc",
      "base/threading/thread_restrictions.cc",
      "base/threading/thread_task_runner_handle.cc",
      "base/threading/worker_pool.cc",
      "base/threading/worker_pool_posix.cc",
      "base/timer/elapsed_timer.cc",
      "base/timer/timer.cc",
      "base/time/clock.cc",
      "base/time/default_clock.cc",
      "base/time/default_tick_clock.cc",
      "base/time/tick_clock.cc",
      "base/time/time.cc",
      "base/time/time_posix.cc",
      "base/trace_event/category_registry.cc",
      "base/trace_event/event_name_filter.cc",
      "base/trace_event/malloc_dump_provider.cc",
      "base/trace_event/heap_profiler_allocation_context.cc",
      "base/trace_event/heap_profiler_allocation_context_tracker.cc",
      "base/trace_event/heap_profiler_allocation_register.cc",
      "base/trace_event/heap_profiler_allocation_register_posix.cc",
      "base/trace_event/heap_profiler_event_filter.cc",
      "base/trace_event/heap_profiler_heap_dump_writer.cc",
      "base/trace_event/heap_profiler_stack_frame_deduplicator.cc",
      "base/trace_event/heap_profiler_type_name_deduplicator.cc",
      "base/trace_event/memory_allocator_dump.cc",
      "base/trace_event/memory_allocator_dump_guid.cc",
      "base/trace_event/memory_dump_manager.cc",
      "base/trace_event/memory_dump_provider_info.cc",
      "base/trace_event/memory_dump_request_args.cc",
      "base/trace_event/memory_dump_scheduler.cc",
      "base/trace_event/memory_dump_session_state.cc",
      "base/trace_event/memory_infra_background_whitelist.cc",
      "base/trace_event/process_memory_dump.cc",
      "base/trace_event/process_memory_maps.cc",
      "base/trace_event/process_memory_totals.cc",
      "base/trace_event/trace_buffer.cc",
      "base/trace_event/trace_config.cc",
      "base/trace_event/trace_config_category_filter.cc",
      "base/trace_event/trace_event_argument.cc",
      "base/trace_event/trace_event_filter.cc",
      "base/trace_event/trace_event_impl.cc",
      "base/trace_event/trace_event_memory_overhead.cc",
      "base/trace_event/trace_event_synthetic_delay.cc",
      "base/trace_event/trace_log.cc",
      "base/trace_event/trace_log_constants.cc",
      "base/tracked_objects.cc",
      "base/tracking_info.cc",
      "base/unguessable_token.cc",
      "base/values.cc",
      "base/version.cc",
      "base/vlog.cc",
    ]
  },

  {
    name = "base-dl"
    output_name = name + "-${libbase_ver}"
    deps = [ ":base-core" ]
    libs = [ "dl" ]
    sources = [ "base/native_library_posix.cc" ]
  },

  {
    name = "base-policy"
    output_name = name + "-${libbase_ver}"
    deps = [ ":base-core" ]
    sources = [
      "components/policy/core/common/policy_load_status.cc",
      "components/policy/core/common/registry_dict.cc",
    ]
  },

  {
    name = "base-base_test_support"
    output_name = name + "-${libbase_ver}"
    testonly = true
    sources = [
      "base/test/fuzzed_data_provider.cc",
      "base/test/simple_test_clock.cc",
      "base/test/simple_test_tick_clock.cc",
      "base/test/test_file_util.cc",
      "base/test/test_file_util_linux.cc",
      "base/test/test_mock_time_task_runner.cc",
      "base/test/test_pending_task.cc",
      "base/test/test_switches.cc",
      "base/test/test_timeouts.cc",
    ]
  }
]

if (use.crypto) {
  libbase_sublibs += [
    {
      name = "base-crypto"
      output_name = name + "-${libbase_ver}"
      deps = [ ":base-core", ":base-dl" ]
      pkg_deps = [ "nss", "openssl" ]
      sources = [
        "crypto/hmac.cc",
        "crypto/hmac_nss.cc",
        "crypto/nss_key_util.cc",
        "crypto/nss_util.cc",
        "crypto/openssl_util.cc",
        "crypto/p224.cc",
        "crypto/p224_spake.cc",
        "crypto/random.cc",
        "crypto/rsa_private_key.cc",
        "crypto/rsa_private_key_nss.cc",
        "crypto/scoped_test_nss_db.cc",
        "crypto/secure_hash.cc",
        "crypto/secure_util.cc",
        "crypto/sha2.cc",
        "crypto/signature_creator_nss.cc",
        "crypto/signature_verifier_nss.cc",
        "crypto/symmetric_key_nss.cc",
        "crypto/third_party/nss/rsawrapr.c",
        "crypto/third_party/nss/sha512.cc",
      ]
    }
  ]
}

if (use.dbus) {
  libbase_sublibs += [
    {
      name = "base-dbus"
      output_name = name + "-${libbase_ver}"
      deps = [ ":base-core" ]
      pkg_deps = [ "dbus-1", "protobuf-lite" ]
      sources = [
        "dbus/bus.cc",
        "dbus/dbus_statistics.cc",
        "dbus/exported_object.cc",
        "dbus/message.cc",
        "dbus/object_manager.cc",
        "dbus/object_path.cc",
        "dbus/object_proxy.cc",
        "dbus/property.cc",
        "dbus/scoped_dbus_error.cc",
        "dbus/string_util.cc",
        "dbus/util.cc",
        "dbus/values_util.cc",
      ]
    },

    {
      name = "base-dbus_test_support"
      output_name = name + "-${libbase_ver}"
      testonly = true
      pkg_deps = [ "dbus-1", "protobuf-lite" ]
      sources = [
        "dbus/mock_bus.cc",
        "dbus/mock_exported_object.cc",
        "dbus/mock_object_manager.cc",
        "dbus/mock_object_proxy.cc",
      ]
    },
  ]
}

if (use.timers) {
  libbase_sublibs += [
    {
      name = "base-timers"
      output_name = name + "-${libbase_ver}"
      deps = [ ":base-core" ]
      sources = [ "components/timers/alarm_timer_chromeos.cc" ]
    },

    {
      name = "base-timer_test_support"
      output_name = name + "-${libbase_ver}"
      testonly = true
      sources = [ "base/timer/mock_timer.cc" ]
    },
  ]
}

# Generate static/shared libraries.
foreach(attr, libbase_sublibs) {
  if (defined(attr.pkg_deps)) {
    # If it depends on external packages, introduces -pkg-config config.
    pkg_config(attr.name + "-pkg-config") {
      pkg_deps = attr.pkg_deps
    }
  }

  if (defined(attr.testonly) && attr.testonly) {
    buildtype = "static_library"
  } else {
    buildtype = "shared_library"
  }
  target(buildtype, attr.name) {
    output_name = attr.output_name
    sources = attr.sources
    if (defined(attr.deps)) {
      deps = attr.deps
    }

    if (defined(attr.libs)) {
      libs = attr.libs
    }

    if (defined(attr.pkg_deps)) {
      configs += [ ":" + attr.name + "-pkg-config" ]
    }
    configs += [
      ":libchrome_config",
      "//common-mk:visibility_default"
    ]
    if (buildtype == "static_library") {
      configs -= [ "//common-mk:use_thin_archive" ]
      configs += [ "//common-mk:nouse_thin_archive" ]
    }
    if (defined(attr.configs)) {
      configs += attr.configs
    }
  }
}

action("base") {
  deps = []
  foreach(attr, libbase_sublibs) {
    if (!defined(attr.testonly) || !attr.testonly) {
      deps += [ ":" + attr.name ]
    }
  }

  script = "//common-mk/write_args.py"
  outputs = [ "${root_out_dir}/lib/lib${target_name}-${libbase_ver}.so" ]
  args = [ "--output" ] + outputs + [ "--" ] + [
    "GROUP", "(", "AS_NEEDED", "(",
  ]
  foreach(attr, libbase_sublibs) {
    if (!defined(attr.testonly) || !attr.testonly) {
      args += [ "-l" + attr.output_name ]
    }
  }
  args += [ ")", ")" ]
}

libchrome_exported_cflags = [
    "-I/usr/include/base-${libbase_ver}",
    "-Wno-unused-local-typedefs",
    "-DBASE_VER=${libbase_ver}",
]

if (use.asan) {
  libchrome_exported_cflags += [ "-DLEAK_SANITIZER" ]
}

generate_pkg_config("libchrome") {
  deps = [ ":base" ]
  output_name = "libchrome-${libbase_ver}"
  description = "chrome base library"
  version = "${libbase_ver}"
  requires_private = []
  foreach(attr, libbase_sublibs) {
    if ((!defined(attr.testonly) || !attr.testonly)
        && defined(attr.pkg_deps)) {
      requires_private += attr.pkg_deps
    }
  }
  libs = [ "-lbase-${libbase_ver}" ]
  libs_private = []
  foreach(attr, libbase_sublibs) {
    if (!defined(attr.testonly) || !attr.testonly) {
      libs_private += [ "-l" + attr.output_name ]
      if (defined(attr.libs)) {
        foreach(lib, attr.libs) {
          libs_private += [ "-l" + lib ]
        }
      }
    }
  }
  cflags = libchrome_exported_cflags
}

action("base-test") {
  deps = []
  foreach(attr, libbase_sublibs) {
    if (defined(attr.testonly) && attr.testonly) {
      deps += [ ":" + attr.name ]
    }
  }

  script = "//common-mk/write_args.py"
  outputs = [ "${root_out_dir}/lib${target_name}-${libbase_ver}.a" ]
  args = [ "--output" ] + outputs + [ "--" ] + [
    "GROUP", "(", "AS_NEEDED", "(",
  ]
  foreach(attr, libbase_sublibs) {
    if (defined(attr.testonly) && attr.testonly) {
      args += [ "-l" + attr.output_name ]
    }
  }
  args += [ ")", ")" ]
}

generate_pkg_config("libchrome-test") {
  deps = [ ":base-test" ]
  output_name = "libchrome-test-${libbase_ver}"
  description = "chrome base test library"
  version = "${libbase_ver}"
  requires_private = []
  foreach(attr, libbase_sublibs) {
    if (defined(attr.testonly) && attr.testonly && defined(attr.pkg_deps)) {
      requires_private += attr.pkg_deps
    }
  }
  libs = [ "-lbase-test-${libbase_ver}" ]
  libs_private = []
  foreach(attr, libbase_sublibs) {
    if (defined(attr.testonly) && attr.testonly) {
      libs_private += [ "-l" + attr.output_name ]
      if (defined(attr.libs)) {
        libs_private += [ "-l" + lib ]
      }
    }
  }
  cflags = libchrome_exported_cflags
}

if (use.mojo) {
  generate_mojom_bindings_gen("mojom_bindings_gen") {
    mojom_bindings_generator = "mojo/public/tools/bindings/mojom_bindings_generator.py"
    sources = [
      "ipc/ipc.mojom",
      "mojo/common/file.mojom",
      "mojo/common/file_path.mojom",
      "mojo/common/string16.mojom",
      "mojo/common/text_direction.mojom",
      "mojo/common/time.mojom",
      "mojo/common/unguessable_token.mojom",
      "mojo/common/values.mojom",
      "mojo/common/version.mojom",
      "mojo/public/interfaces/bindings/pipe_control_messages.mojom",
      "mojo/public/interfaces/bindings/interface_control_messages.mojom",
    ]
  }

  # Probably we should consider build libmojo as a part of libchrome.
  # crbug.com/924035.
  static_library("mojo") {
    output_name = "mojo-${libbase_ver}"
    deps = [ ":base-core", ":base-crypto", ":mojom_bindings_gen" ]
    # TODO(hidehiko): Consolidate with build_config.h.
    configs -= [
      "//common-mk:use_thin_archive",
      "//common-mk:pie",
    ]
    configs += [
      ":libchrome_config",
      "//common-mk:visibility_default",
      "//common-mk:nouse_thin_archive",
      "//common-mk:pic",
    ]
    sources = [
      "ipc/ipc_message.cc",
      "ipc/ipc_message_attachment.cc",
      "ipc/ipc_message_attachment_set.cc",
      "ipc/ipc_message_utils.cc",
      "ipc/ipc_mojo_handle_attachment.cc",
      "ipc/ipc_mojo_message_helper.cc",
      "ipc/ipc_mojo_param_traits.cc",
      "ipc/ipc_platform_file_attachment_posix.cc",
      "mojo/common/common_custom_types_struct_traits.cc",
      "mojo/common/data_pipe_drainer.cc",
      "mojo/common/data_pipe_utils.cc",
      "mojo/common/values_struct_traits.cc",
      "mojo/edk/embedder/connection_params.cc",
      "mojo/edk/embedder/embedder.cc",
      "mojo/edk/embedder/entrypoints.cc",
      "mojo/edk/embedder/named_platform_handle_utils_posix.cc",
      "mojo/edk/embedder/pending_process_connection.cc",
      "mojo/edk/embedder/platform_channel_pair.cc",
      "mojo/edk/embedder/platform_channel_pair_posix.cc",
      "mojo/edk/embedder/platform_channel_utils_posix.cc",
      "mojo/edk/embedder/platform_handle.cc",
      "mojo/edk/embedder/platform_handle_utils_posix.cc",
      "mojo/edk/embedder/platform_shared_buffer.cc",
      "mojo/edk/embedder/scoped_ipc_support.cc",
      "mojo/edk/embedder/test_embedder.cc",
      "mojo/edk/system/broker_host.cc",
      "mojo/edk/system/broker_posix.cc",
      "mojo/edk/system/channel.cc",
      "mojo/edk/system/channel_posix.cc",
      "mojo/edk/system/configuration.cc",
      "mojo/edk/system/core.cc",
      "mojo/edk/system/data_pipe_consumer_dispatcher.cc",
      "mojo/edk/system/data_pipe_control_message.cc",
      "mojo/edk/system/data_pipe_producer_dispatcher.cc",
      "mojo/edk/system/dispatcher.cc",
      "mojo/edk/system/handle_table.cc",
      "mojo/edk/system/mapping_table.cc",
      "mojo/edk/system/message_for_transit.cc",
      "mojo/edk/system/message_pipe_dispatcher.cc",
      "mojo/edk/system/node_channel.cc",
      "mojo/edk/system/node_controller.cc",
      "mojo/edk/system/platform_handle_dispatcher.cc",
      "mojo/edk/system/ports/event.cc",
      "mojo/edk/system/ports/message.cc",
      "mojo/edk/system/ports/message_queue.cc",
      "mojo/edk/system/ports/name.cc",
      "mojo/edk/system/ports/node.cc",
      "mojo/edk/system/ports/port.cc",
      "mojo/edk/system/ports/port_ref.cc",
      "mojo/edk/system/ports_message.cc",
      "mojo/edk/system/request_context.cc",
      "mojo/edk/system/shared_buffer_dispatcher.cc",
      "mojo/edk/system/watch.cc",
      "mojo/edk/system/watcher_dispatcher.cc",
      "mojo/edk/system/watcher_set.cc",
      "mojo/public/c/system/set_thunks_for_app.cc",
      "mojo/public/c/system/thunks.cc",
      "mojo/public/cpp/bindings/lib/array_internal.cc",
      "mojo/public/cpp/bindings/lib/associated_binding.cc",
      "mojo/public/cpp/bindings/lib/associated_group.cc",
      "mojo/public/cpp/bindings/lib/associated_group_controller.cc",
      "mojo/public/cpp/bindings/lib/associated_interface_ptr.cc",
      "mojo/public/cpp/bindings/lib/binding_state.cc",
      "mojo/public/cpp/bindings/lib/connector.cc",
      "mojo/public/cpp/bindings/lib/control_message_handler.cc",
      "mojo/public/cpp/bindings/lib/control_message_proxy.cc",
      "mojo/public/cpp/bindings/lib/filter_chain.cc",
      "mojo/public/cpp/bindings/lib/fixed_buffer.cc",
      "mojo/public/cpp/bindings/lib/interface_endpoint_client.cc",
      "mojo/public/cpp/bindings/lib/message.cc",
      "mojo/public/cpp/bindings/lib/message_buffer.cc",
      "mojo/public/cpp/bindings/lib/message_builder.cc",
      "mojo/public/cpp/bindings/lib/message_header_validator.cc",
      "mojo/public/cpp/bindings/lib/multiplex_router.cc",
      "mojo/public/cpp/bindings/lib/native_struct.cc",
      "mojo/public/cpp/bindings/lib/native_struct_data.cc",
      "mojo/public/cpp/bindings/lib/native_struct_serialization.cc",
      "mojo/public/cpp/bindings/lib/pipe_control_message_handler.cc",
      "mojo/public/cpp/bindings/lib/pipe_control_message_proxy.cc",
      "mojo/public/cpp/bindings/lib/scoped_interface_endpoint_handle.cc",
      "mojo/public/cpp/bindings/lib/serialization_context.cc",
      "mojo/public/cpp/bindings/lib/string_traits_string16.cc",
      "mojo/public/cpp/bindings/lib/sync_call_restrictions.cc",
      "mojo/public/cpp/bindings/lib/sync_event_watcher.cc",
      "mojo/public/cpp/bindings/lib/sync_handle_registry.cc",
      "mojo/public/cpp/bindings/lib/sync_handle_watcher.cc",
      "mojo/public/cpp/bindings/lib/validation_context.cc",
      "mojo/public/cpp/bindings/lib/validation_errors.cc",
      "mojo/public/cpp/bindings/lib/validation_util.cc",
      "mojo/public/cpp/system/buffer.cc",
      "mojo/public/cpp/system/platform_handle.cc",
      "mojo/public/cpp/system/simple_watcher.cc",
      "mojo/public/cpp/system/wait.cc",
      "mojo/public/cpp/system/wait_set.cc",
      "mojo/public/cpp/system/watcher.cc",
    ] + get_target_outputs(":mojom_bindings_gen")
  }

  generate_pkg_config("libmojo") {
    deps = [ ":mojo" ]
    output_name = "libmojo-${libbase_ver}"
    description = "Chrome Mojo IPC library"
    version = "${libbase_ver}"
    libs = [ "-lmojo-${libbase_ver}" ]
    cflags = [
      "-I/usr/lib/base-${libbase_ver}",
      "-Wno-cast-qual",
      "-Wno-cast-align",
    ]
  }
}
