# Copyright 2014 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

# Since we execute pbzip2 outside of the chroot, we need to statically
# link this to avoid library mismatch errors.  http://crosbug.com/32519
app-arch/bzip2 static-libs
app-arch/pbzip2 static
# Same for pigz.
app-arch/pigz static
sys-libs/zlib static-libs

# Drop this once we audit code to make sure we aren't using `echo -n` or `echo -e`.
app-shells/dash	vanilla

net-misc/openssh kerberos hpn
sys-apps/flashrom dediprog ft2232_spi serprog
dev-lang/python gdbm
sys-fs/lvm2 -thin

# qemu is statically linked with glib when compiled for the host and thus
# requires glib (and transitively, libpcre) to be compiled with static-libs.
# See http://crosbug.com/35162 for details.
dev-libs/glib static-libs
dev-libs/libpcre static-libs
sys-apps/attr static-libs

# mksquashfs used in build_image requires lzo support.
# Also include lz4 and lzma compressors, for flexibility.
# For SELinux context labeling it also needs selinux.
sys-fs/squashfs-tools lzo lz4 lzma selinux

# some prebuilt binaries created on Ubuntu like to link against libtinfo.
sys-libs/ncurses tinfo

# icedtea-bin still has some linking issues when built without USE=cups. See
# https://bugs.gentoo.org/486042 and https://crbug.com/656717
dev-java/icedtea-bin cups -gtk headless-awt -multilib -webstart
# We don't want cups-filters to pull in ghostscript.
net-print/cups-filters -postscript

# Disable Segger J-Link for building openocd-0.10.0. We currently
# don't use J-Link. It requires upgrading libjaylink, which is not yet
# supported by official Gentoo portage repository.
dev-embedded/openocd	-jlink

# Provide 32-lib libc++ libraries.
# https://crbug.com/756528
sys-libs/libcxxabi abi_x86_32
sys-libs/libcxx abi_x86_32
sys-libs/llvm-libunwind abi_x86_32

# vaapi is not used through ffmpeg on ChromiumOS, avoid dependency for sdk
media-video/ffmpeg -vaapi

# Enable XML in GDB
sys-devel/gdb	xml
cross-aarch64-cros-linux-gnu/gdb	xml
cross-arm-none-eabi/gdb	xml
cross-armv6j-cros-linux-gnueabi/gdb	xml
cross-armv7a-cros-linux-gnueabi/gdb	xml
cross-armv7a-cros-linux-gnueabihf/gdb	xml
cross-i686-pc-linux-gnu/gdb	xml
cross-mipsel-cros-linux-gnu/gdb	xml
cross-x86_64-cros-linux-gnu/gdb	xml

# Build qemu's userland helpers statically so we can copy them into sysroots
# and run unittests standalone.  Build qemu's softmmu helpers statically so
# we can run vms outside of the chroot.
app-emulation/qemu	-pin-upstream-blobs -seccomp static-user static usb
dev-libs/glib		static-libs
dev-libs/libaio		static-libs
dev-libs/libpcre	static-libs
dev-libs/libusb		static-libs
dev-libs/libxml2	static-libs
dev-libs/openssl	static-libs
media-libs/jpeg		static-libs
media-libs/libjpeg-turbo	static-libs
media-libs/libpng	static-libs
net-misc/curl		static-libs
sys-apps/attr		static-libs
sys-apps/dtc		static-libs
sys-libs/libcap-ng	static-libs
sys-libs/ncurses	static-libs unicode
virtual/jpeg		static-libs
virtual/libusb		static-libs
x11-libs/libxkbcommon	static-libs
x11-libs/pixman		static-libs

# Enable Python3.6 support for some packages. After we have upgraded each
# Python3-capable package to support Python3.6, we should add python3_6 to
# PYTHON_TARGETS and delete this block.
#
# TODO(chadversary): Delete this block when PYTHON_TARGETS has python3_6.
# See <http://crbug.com/926296>.
dev-util/meson python_targets_python3_6
dev-python/setuptools python_targets_python3_6
dev-python/certifi python_targets_python3_6
dev-python/mako python_targets_python3_6
dev-python/markupsafe python_targets_python3_6

# Disable older python in packages that depend on portage to break circular rebuilds.
dev-java/java-config -python_targets_python3_4

# Lock in old python versions on existing binary packages.
<=sys-devel/gdb-8.0.1.20180820-r1	python_targets_python3_4 -python_targets_python3_6
<=cross-aarch64-cros-linux-gnu/gdb-8.0.1.20180820-r1	python_targets_python3_4 -python_targets_python3_6
<=cross-arm-none-eabi/gdb-8.0.1.20180820-r1	python_targets_python3_4 -python_targets_python3_6
<=cross-armv6j-cros-linux-gnueabi/gdb-8.0.1.20180820-r1	python_targets_python3_4 -python_targets_python3_6
<=cross-armv7a-cros-linux-gnueabihf/gdb-8.0.1.20180820-r1	python_targets_python3_4 -python_targets_python3_6
<=cross-armv7m-cros-eabi/gdb-8.0.1.20180820-r1	python_targets_python3_4 -python_targets_python3_6
<=cross-i686-pc-linux-gnu/gdb-8.0.1.20180820-r1	python_targets_python3_4 -python_targets_python3_6
<=cross-x86_64-cros-linux-gnu/gdb-8.0.1.20180820-r1	python_targets_python3_4 -python_targets_python3_6

# Enable building shellcheck statically so we can make it available to tricium
# and developer workstations.
dev-libs/gmp		static-libs
dev-libs/libffi		static-libs

# MariaDB connector must run in MySQL compatible mode in the SDK for autotest
dev-db/mariadb-connector-c  mysqlcompat
