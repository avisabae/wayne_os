From d0ddb709b8a40f62713bf75da746c8474871dab9 Mon Sep 17 00:00:00 2001
From: Bing Xue <bingxue@google.com>
Date: Thu, 17 Jan 2019 14:43:00 -0800
Subject: [PATCH] tensorflow: Add target for extra headers to nnapi

The "instal_nnapi_extra_headers" target is used for exporting additional
headers required to build some nnapi implementation that uses
optmized_ops and reference_ops.

The TFLite team intend to support such a target in the future.
crbug.com/924331 is tracking this effort on our side.

The tensorflow upstream patch corresponding to this patch is at:
https://github.com/tensorflow/tensorflow/commit/81cf135cd771d774680633ea42bbf3bf8cf1e603#diff-16cf5651302bf6046505fe60ae8f9e2e
---
 .../contrib/lite/kernels/internal/BUILD       | 70 +++++++++++++++++++
 1 file changed, 70 insertions(+)

diff --git a/tensorflow/contrib/lite/kernels/internal/BUILD b/tensorflow/contrib/lite/kernels/internal/BUILD
index 0a5223b235..67ce605c5e 100644
--- a/tensorflow/contrib/lite/kernels/internal/BUILD
+++ b/tensorflow/contrib/lite/kernels/internal/BUILD
@@ -6,6 +6,7 @@ licenses(["notice"])  # Apache 2.0
 
 load("//tensorflow/contrib/lite:build_def.bzl", "tflite_copts")
 load("//tensorflow/contrib/lite:special_rules.bzl", "tflite_portable_test_suite")
+load("//tensorflow:tensorflow.bzl", "tf_cc_test", "transitive_hdrs")
 
 tflite_deps_intel = [
     "@arm_neon_2_x86_sse",
@@ -553,4 +554,73 @@ cc_test(
 
 exports_files(["optimized/eigen_tensor_reduced_instantiations_oss.h"])
 
+filegroup(
+    name = "optimized_op_headers",
+    srcs = glob([
+        "optimized/*.h",
+    ]),
+    visibility = ["//tensorflow/contrib/lite:__subpackages__"],
+)
+
+filegroup(
+    name = "reference_op_headers",
+    srcs = glob([
+        "reference/*.h",
+    ]),
+    visibility = ["//tensorflow/contrib/lite:__subpackages__"],
+)
+
+filegroup(
+    name = "headers",
+    srcs = glob([
+        "*.h",
+    ]),
+    visibility = ["//tensorflow/contrib/lite:__subpackages__"],
+)
+
+transitive_hdrs(
+    name = "nnapi_external_headers",
+    visibility = ["//tensorflow/contrib/lite:__subpackages__"],
+    deps = [
+        "//third_party/eigen3",
+        "@gemmlowp",
+    ],
+)
+
+genrule(
+    name = "install_nnapi_extra_headers",
+    srcs = [
+        ":nnapi_external_headers",
+        ":headers",
+        ":optimized_op_headers",
+        ":reference_op_headers",
+    ],
+    outs = ["include"],
+    cmd = """
+    mkdir $@
+    for f in $(SRCS); do
+      d="$${f%/*}"
+      d="$${d#bazel-out*genfiles/}"
+      d="$${d#*external/eigen_archive/}"
+
+      if [[ $${d} == *local_config_* ]]; then
+        continue
+      fi
+
+      if [[ $${d} == external* ]]; then
+        extname="$${d#external/}"
+        extname="$${extname%%/*}"
+        if [[ $${TF_SYSTEM_LIBS:-} == *$${extname}* ]]; then
+          continue
+        fi
+      fi
+
+      mkdir -p "$@/$${d}"
+      cp "$${f}" "$@/$${d}/"
+    done
+    """,
+    tags = ["manual"],
+    visibility = ["//visibility:private"],
+)
+
 tflite_portable_test_suite()
-- 
2.20.1.791.gb4d0f1c61a-goog

