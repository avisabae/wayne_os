logitech-updater is a utility to upgrade logitech camera firmwares.

## Requirements
The GNU C/C++ library is required.

## Building
At the top level of the directory.
```
$ make
```
Alternatively at Chromium OS development environment,
```
$ emerge-${BOARD} logitech-updater
```

## How to use
```
logitech updater currently includes the follow executables: ptzpro2-updater
$ ptzpro2-updater -h
```

