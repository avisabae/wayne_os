%# Copyright (c) 2012 The Chromium OS Authors. All rights reserved.
%# Use of this source code is governed by a BSD-style license that can be
%# found in the LICENSE file.

%from src import settings
%_root = settings.settings.relative_root

%def body_block():
  %# --------------------------------------------------------------------------
  %# Releases switcher
  %include('switcher_bar.tpl', tpl_vars=tpl_vars, query_string=query_string, url_base='comments')

  %# --------------------------------------------------------------------------
  %# Comments
  <script>
    %# """ Defines the data model used by AngularJS for template suggestions.
    %#
    %# We will offer simple template suggestions for comments. This will aid
    %# standardized workflow, queryability and issue awareness.
    %#
    %# """
    app.controller('MyController', function($scope, $http, $location) {
      $scope.data = [
        'Triaged',
        'Investigating',
        'Started',
        'crosbug.com/_bugid_',
        'crbug.com/_bugid_'
      ];

      $scope.author = null;
      $scope.comment = null;
      $scope.test_id = '{{ tpl_vars['data']['test_id'] }}';

      $scope.filterComment = function() {
        %# returns a list of matching candidates.
        var data = $scope.data;
        var filtered = [];
        var query = $scope.comment;

        if (!query || String(query).trim().length == 0) {
          return;
        }
        var queryRegExp = RegExp(query, 'i'); //'i' -> case insensitive
        angular.forEach(data, function(v) {
          if (v.match(queryRegExp)) {
            filtered.push(v);
          }
        });
        return filtered;
      }

      $scope.selectTemplate = function(item) {
        %# Set comment to item.
        $scope.comment = item;
      }

      $scope.postComment = function() {
        %# Post the comment for update in the db.
        if (!$scope.comment || $scope.comment.trim().length == 0) {
            return;
        }
        if ($scope.author && $scope.author.trim().length > 0) {
            $scope.comment = ($scope.author + ': ') + $scope.comment;
        }
        var formData = $.param({"comment": $scope.comment,
                                "test_id": $scope.test_id});
        $http.post("{{ _root }}/comments", formData).
            success(function(responseData) {
                window.location.reload();
            }).
            error(function(responseData, status) {
                alert('Not added: error ' + status + '.');
            });
      }
    });
  </script>
  <div id="divComments" align="center">
    %#-------------------------------------------------------------------------
    %# New Comment entry area.
    %#-------------------------------------------------------------------------
    <div class="lefted" ng-controller="MyController">
      <textarea class="textarea_wide" ng-model="comment" rows="5"
                placeholder="Enter a new triage comment here..."></textarea>
      <br>
      <ul>
        <li class="infotext" ng-repeat="item in filterComment()">
          [ <a href="" ng-click="selectTemplate(item)">select</a> ]
          {[{ item }]}
        </li>
      </ul>
      <input ng-model="author" cols="20"
                placeholder="Add author here..."></input>
      <a class="maia-button" ng-click="postComment()">
        Add new comment
      </a>

      %_issue_link = tpl_vars['data']['issue_link']
      %if _issue_link:
        <a class="maia-button" href="{{ _issue_link }}" target="_blank">
          Report Bug for this Problem
        </a>
      %end

      %_research_link = tpl_vars['data']['research_link']
      %if _research_link:
        <a class="maia-button" href="{{ _research_link }}" target="_blank">
          Search Tracker for Related Issues
        </a>
      %end
    </div>

    %#-------------------------------------------------------------------------
    %# A table with test details (no comments).
    %#-------------------------------------------------------------------------
    %_top_title = 'Test Details'
    %_cell_classes = {'_default': 'failure_table centered',
    %                 'comment': 'failure_table lefted',
    %                 'reason': 'failure_table lefted'}
    %_head_classes = {'_default': 'headeritem centered',
    %                 'comment': 'headeritem lefted',
    %                 'reason': 'headeritem lefted'}
    %_test_td = tpl_vars['data']['test_td']
    {{! _test_td.simple_table(_top_title, _cell_classes, _head_classes) }}

    %#-------------------------------------------------------------------------
    %# History of Comments on this Test ID.
    %#-------------------------------------------------------------------------
    %_comment_td = tpl_vars['data']['comment_td']
    %if not _comment_td.row_headers:
      %_top_head = 'No Previous Comments on this Test.'
    %else:
      %_top_head = 'Comment History'
    %end
    {{! _comment_td.simple_table(_top_head, _cell_classes, _head_classes) }}

    %#-------------------------------------------------------------------------
    %# Comments on Related Tests (same name different id, recent).
    %#-------------------------------------------------------------------------
    %_related_td = tpl_vars['data']['related_td']
    %if not _related_td.row_headers:
      %_top_head = 'No Related Comments to this Test.'
    %else:
      %_top_head = 'Comments - Related Tests'
    %end
    {{! _related_td.simple_table(_top_head, _cell_classes, _head_classes) }}
  </div>
  <hr>
%end

%rebase('master.tpl', title='comments', query_string=query_string, body_block=body_block, angular=True)
