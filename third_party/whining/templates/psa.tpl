%# Copyright 2016 The Chromium OS Authors. All rights reserved.
%# Use of this source code is governed by a BSD-style license that can be
%# found in the LICENSE file.

%# Template for displaying a message in a big pink banner above main content.
%# Useful in case of troubles. Change to "if True:" to enable.

%if False:
  <div class="psa_banner">
    Experiencing _xyz_ issues, investigating -
    <a href="http://crbug.com/123123">crbug.com/123123</a>
  </div>
%end

% from src import settings
%if settings.settings.stainless_url:
  <div class="stainless_banner">
    wmatrix is being deprecated.
    Please use <a href="{{ settings.settings.stainless_url }}">Stainless</a> instead.
  </div>
%end
