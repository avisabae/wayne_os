%# Copyright (c) 2012 The Chromium OS Authors. All rights reserved.
%# Use of this source code is governed by a BSD-style license that can be
%# found in the LICENSE file.
%import pprint
%def body_block():
  <h2 style="text-align:center">Chromium OS Test Results (Whining)</h2>
  <div id="body_block">
  %if not tpl_vars:
    <h3><u>No test results found.</u></h3>
  %end

  %# --------------------------------------------------------------------------
  %# Show master data structure
  <table class="bordereditem">
  <tbody>
  %alternate = True
  %for k in sorted(tpl_vars['test_results'].keys(), reverse=True):
    <tr><td class="bordereditem">*debugrow*<td></tr>
    <tr>
      <td class="bordereditem headeritem">{{ pprint.pformat(k) }}</td>
    </tr>
    <tr>
      <td class="bordereditem">
        %if alternate:
        <table class="alternate_background bordereditem">
        %else:
        <table class="bordereditem">
        %end
          <tbody>
          %for line in pprint.pformat(tpl_vars['test_results'][k]).split('\n'):
            <tr>
              <td class="bordereditem">{{ line }}</td>
            </tr>
          %end
          </tbody>
        </table>
      </td>
    </tr>
    %alternate = not alternate
  %end
  </tbody>
  </table>
  </div>
%end

%rebase('master.tpl', title='all', query_string=query_string, body_block=body_block)
