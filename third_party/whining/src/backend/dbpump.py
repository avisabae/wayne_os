# Copyright (c) 2013 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

""" Tools for pumping data between databases.
"""

import logging
import time

import pytz
import MySQLdb

from src import settings
from src import wmutils
from src.models import db_interface


# When initializing an empty DB, how many days of data to copy.
# When > ~10, copying may take long time
INIT_DAYS_BACK = 1

# The time zone of the source DB. Destination is always in UTC.
SRC_TZINFO = pytz.timezone('America/Los_Angeles')


class TimeConverter(object):
    """Convert all columns that have 'time' in their name from PT to UTC.

    This converter is instantiated by the pump_table() function that can
    take a converter class as a parameter.

    Used for tko_tests and tko_jobs tables.
    pytz is a third party lib, for details see: http://pytz.sourceforge.net/

    MySQL DATETIME data type does not include any timezone data, it can be
    treated as a tuple (year, month, day, hour, minute, second, us) with no
    knowledge of what timezone this time is in. In terms used by python
    datetime module, this is the same as a 'naive' datetime object.

    MySQL function to get current UTC time is UTC_TIMESTAMP()
    Other functions like now(), curtime() etc. will, by default, return time
    in server's local timezone which can be found as:
    SELECT @@global.system_time_zone, @@session.time_zone;
    """
    # TODO:(kamrik) talk to scottz@ about storing all their times in UTC.
    #     the TIMESTAMP data type in MySQL is internally stored as UTC.

    def __init__(self, cols, db):
        tzinfo = SRC_TZINFO
        self.tzinfo = tzinfo
        self.cols = cols
        self.time_cols = [i for (i, col) in enumerate(cols) if 'time' in col]
        ucols = ['reason']
        self.utf8_cols = [i for (i, col) in enumerate(cols) if col in ucols]
        self.new_cols = cols[:]
        self.db = db

    def convert(self, rows):
        # TODO (kamrik): check how much mem this uses, try iterators.
        tz = self.tzinfo
        new_rows = []
        for row in rows:
            row = list(row)
            for idx in self.time_cols:
                dt = row[idx]
                if dt:
                    row[idx] = tz.localize(dt).astimezone(pytz.utc)
            for idx in self.utf8_cols:
                row[idx] = unicode(row[idx], 'latin1')
            new_rows.append(row)
        return new_rows


class JobsConverter(TimeConverter):
    """An object that transforms data from tko_jobs table.

    This converter is instantiated by the pump_table() function that can
    take a converter class as a parameter.

    Example of a job row after conversion:
    job = {
        'afe_job_id': 1584988,
        'build': 'R00026-03601.00000.00000',
        'build_config': 'daisy-release',
        'build_maintenance': 0,
        'build_major': 3601,
        'build_minor': 0,
        'build_remaining': '',
        'buildname': 'R26-3601.0.0',
        'finished_time': datetime.datetime(2013, 1, 25, 2, 31, 52),
        'job_idx': 1674487,
        'label': 'daisy-release/R26-3601.0.0/bvt/network_DisableInterface',
        'machine_idx': 2408,
        'queued_time': datetime.datetime(2013, 1, 25, 1, 49, 35),
        'release_number': 26,
        'started_time': datetime.datetime(2013, 1, 25, 2, 31, 5),
        'suite': 'bvt',
        'tag': '1584988-moblab/192.168.231.100',
        'test_lbl': 'network_DisableInterface',
        'username': 'moblab',
        'fw_rw_version': daisy-firmware/R21-3001.0.0,
        'fw_ro_version': NULL,
        'test_version': daisy-release/R26-3601.0.0,
    }
    """

    def __init__(self, cols, db):
        super(JobsConverter, self).__init__(cols, db)
        self.cols = cols
        # Rename col build and suite to avoid name collision.
        self.cols[self.cols.index('build')] = 'build_in_tko_jobs'
        self.cols[self.cols.index('suite')] = 'suite_in_tko_jobs'
        # columns added to each row by this converter
        self.extra_cols = [
            'build_config',
            'buildname',
            'suite',
            'test_lbl',
            'build',
            'release_number',
            'build_major',
            'build_minor',
            'build_maintenance',
            'build_remaining',
            'fw_rw_version',
            'fw_ro_version',
            'test_version',
            ]
        # The new_cols property is used by the pump_table function
        self.new_cols = self.cols + self.extra_cols
        self.time_cols = [i for (i, col) in enumerate(cols) if 'time' in col]

    def _get_build_keyvals(self, job_id):
        """Get the build values for a given job_id.

        @param job_id: The imported_tko_jobs.id, not the test job id.

        @return: The values of builds stored in tko_job_keyvals, in the order of
                 [fwrw_build, fwro_build, test_source_build]

        """
        builds = []
        query = ('SELECT tko_job_keyvals.key,value FROM tko_job_keyvals WHERE '
                 'job_id=%(JOB_ID)d')
        results = self.db.run_query_multilst(query % {'JOB_ID': job_id})
        if not results:
            return [None, None, None]

        mapping = {prefix: build for [prefix, build] in results}
        return [mapping.get('fwrw_build'),
                mapping.get('fwro_build'),
                mapping.get('test_source_build')]

    def convert(self, rows):
        idx = self.cols.index('label')
        job_idx = self.cols.index('job_idx')
        build_idx = self.new_cols.index('build')
        a_rows = super(JobsConverter, self).convert(rows)
        # Skip the rows for which we can't parse the label.
        # TODO: Figure out why we have them at all and avoid them in SQLECT?
        b_rows = []
        for row in a_rows:
            try:
                lbl_part = self._parse_label(row[idx])
            except:
                print 'Bad lbl: "%s"' % row[idx]
                continue
            b_rows.append(list(row) + lbl_part +
                          self._get_build_keyvals(row[job_idx]))
        # rows = [list(row) + self._parse_label(row[idx]) for row in rows]
        # Filter out jobs from builds with -b numbers like R23-2913.327.0-b456
        if settings.settings.filter_b_builds:
            b_rows = [row for row in b_rows if row[build_idx].find('-b') == -1]
        return b_rows

    def _parse_label(self, label):
        """Parses the job label field to extract several more useful fields.

        A typical cros job label would look like
        'lumpy-release/R26-3520.0.0/bvt/network_Ping'
        The parts of it are:
         * 'lumpy-release' name of the configuration used by buildbot
         * 'R26-3520.0.0' Chromium OS version
         * 'bvt' test suite name
         * 'network_Ping' test name

        A typical android job label would look like
        'git_mnc-brillo-dev/dragonboard-eng/2660887/brillo-bvt/brillo_test'
        The parts of it are:
         * 'git_mnc-brillo-dev/dragonboard-eng' name of the branch and target.
         * '2660887' Build id
         * 'brillo-bvt' test suite name
         * 'brillo_test' test name
        The version is also split to pars as:
         (release, major, minor, maintenance, the_rest).
        """
        if wmutils.cros_re_build.search(label):
            if label.endswith('-try'):
                # Those are autoupdate/repair/fwupdate jobs. e.g:
                # label = 'lumpy-release/R26-3520.0.0-try'
                suite = 'lab'
                test_name = None
                (config, buildname) = label.split('/', 1)
                buildname = buildname.replace('-try', '')
            else:
                logging.debug('label: %s is cros test label', label)
                (config, buildname, suite, test_name) = label.split('/', 3)
        else:
            logging.debug('label: %s is android test label', label)
            (config, buildname, suite, test_name) = label.rsplit('/', 3)
        logging.debug('buildname: %s', buildname)
        build_parts = wmutils.build2tuple(buildname)
        sortable_build = wmutils.build2sortable(buildname)
        lst = [config, buildname, suite, test_name, sortable_build]
        lst += list(build_parts)
        return lst


# Due to a bug in some MySQLdb versions, the word VALUES in the INSERT clause
# must be lower case for executemany() to treat it as multi-line insert.
# stackoverflow.com/questions/3945642/why-is-executemany-slow-in-python-mysqldb
def insert_clause(table, cols, replace=False):
    """ Generate an INSERT or REPLACE clause.

    Example:
    insert_clause('tbl', ['col_a', 'col_b'])
    INSERT INTO tbl (col_a, col_b) values (%s, %s)
    """
    if replace:
        cmd = 'REPLACE'
    else:
        cmd = 'INSERT'
    str_cols = ', '.join(['`%s`' % c for c in cols])
    percents = ', '.join(['%s'] * len(cols))
    sql = '%s INTO %s (%s) values (%s)'
    sql = sql % (cmd, table, str_cols, percents)
    return sql


def pump_table(src_db, query, dest_db, tbl,
               converter_class=None, maxfetch=3000,
               max_retries=5, seconds_retry=10,
               ss_cursor=True):
    """Run a query in src_db and write the results into dest_db.

    This function runs the |query| in |src_db| and copies the resulting rows
    to table |tbl| in |dest_db|, optionally transformed by an instance of
    |converter_class|. The rows are fetched using a server-side
    cursor, |maxfetch| rows at a time.
    """

    # For multi-line INSERT to work fast we need autocommit to be off.
    dest_db.autocommit(False)
    if ss_cursor:
        scursor = src_db.cursor(cursorclass=MySQLdb.cursors.SSCursor)
    else:
        scursor = src_db.cursor(use_dict=False)
    dcursor = dest_db.cursor(use_dict=False)

    scursor.execute(query)
    cols = [x[0] for x in scursor.description]
    if converter_class:
        # Create a new DBInterface to run querying on tko_job_keyvals. The
        # existing src_db can't be used to run other queries due to SSCursor.
        src_db_2 = db_interface.DBInterface(src_db.settings)
        converter = converter_class(cols, src_db_2)
        cols = converter.new_cols

    insert = insert_clause(tbl, cols)

    start_time = time.time()
    total_rows = 0

    try:
        rows = scursor.fetchmany(maxfetch)
        while rows:
            if converter_class:
                rows = converter.convert(rows)
            msg = "Inserting %s rows. So far copied %d rows in %0.2f seconds"
            logging.info(msg, len(rows), total_rows, time.time() - start_time)
            total_rows += len(rows)
            # Write the rows, retry if failed.
            for attempt in xrange(max_retries + 1):
                try:
                    dcursor.executemany(insert, rows)
                    dest_db.commit()
                    break
                except dest_db.settings.db_module.OperationalError:
                    # To test stop mysql and restart within 'seconds_retry'.
                    if attempt >= max_retries:
                        msg = 'Insert failed for %s times' % max_retries
                        raise Exception(msg)
                    msg = 'Exception while inserting, retrying in %d seconds...'
                    logging.exception(msg, seconds_retry)
                    dcursor.close()
                    time.sleep(seconds_retry)
                    dest_db.connect()
                    dest_db.autocommit(False)
                    dcursor = dest_db.cursor(use_dict=False)

            rows = scursor.fetchmany(maxfetch)

        end_time = time.time()
        logging.info("Finished copying %d rows in %0.3f seconds",
                     total_rows, end_time - start_time)

    finally:
        scursor.close()
        dcursor.close()

    dest_db.autocommit(True)
    return total_rows


def insert_dictlist(dictlist, fields, dest_db, table, replace=False):
    """Insert data given a list of dictionaries into a table."""

    insert = insert_clause(table, fields, replace=replace)

    # Prepare the rows to insert as lists with the same order as db_fields
    rows = [[row[k] for k in fields] for row in dictlist]

    cursor = dest_db.cursor()
    cursor.executemany(insert, rows)
    dest_db.commit()
    cursor.close()


def get_starting_idxs_for_init(src_db, days_back=1):
    """Same as above, but intended for initializing an empty WM db.

    |days_back| tells how many days worth of data to copy from AutoTest db.
    """

    d = {}
    d['DAYS_BACK'] = days_back

    sql = "SELECT adddate(now(),-%(DAYS_BACK)s )" % d
    max_time = src_db.run_query_scalar(sql)
    d['MAX_TIME'] = max_time

    sql = ("SELECT min(job_idx) from tko_jobs "
           "where started_time > adddate(now(),-%(DAYS_BACK)s )"
           ) % d

    job_idx = src_db.run_query_scalar(sql)
    d['JOB_IDX'] = job_idx

    sql = "SELECT min(test_idx) from tko_tests where job_idx=%s" % job_idx
    d['TEST_IDX'] = src_db.run_query_scalar(sql)

    return d


def get_starting_idxs(src_db, dest_db, wmdb=True):
    """Get the job and test indexes to start from when querying AutoTest db.

    Find the latest timestamp of a test record in the dest_db. Then take an
    arbitrary test from DAYS_BACK earlier and the corresponding job.

    Going DAYS_BACK back in time ensures that we update tests that were
    already running but did not yet complete during the previous update.
    """

    d = {}
    # TODO: Fix timestamps in AutoTest DB and set DAYS_BACK back to 1
    d['DAYS_BACK'] = 3

    # Find the latest starting time of a test in destination DB
    if wmdb:
        sql = 'SELECT max(test_started_time) FROM good_tests'
    elif settings.settings.filter_autotest_db:
        # TODO: Fix timestamps in AutoTest DB and delete the entire WHERE
        # clause (will be much faster)
        sql = ('SELECT max(started_time) FROM tko_tests '
               'WHERE machine_idx <> 2300 '
               'AND started_time > adddate(now(), -8)'
              )
    else:
        sql = 'SELECT max(started_time) FROM tko_tests'
    max_time = dest_db.run_query_scalar(sql)
    # for wmdb convert timezone from UTC to PT
    if wmdb:
        max_time_src = pytz.utc.localize(max_time).astimezone(SRC_TZINFO)
        # strip tzinfo to keep the datetime tuple naive as DB expects it
        max_time = max_time_src.replace(tzinfo=None)
    d['MAX_TIME'] = max_time

    # Find the earliest test in source DB that started
    # after MAX_TIME - DAYS BACK
    sql = ("SELECT min(test_idx) from tko_tests "
           "WHERE started_time > adddate('%(MAX_TIME)s',-%(DAYS_BACK)s )"
           ) % d

    d['TEST_IDX'] = src_db.run_query_scalar(sql)
    sql = 'SELECT job_idx from tko_tests where test_idx=%(TEST_IDX)s' % d
    d['JOB_IDX'] = src_db.run_query_scalar(sql)
    logging.info('Starting points are: %s', d)
    return d


if __name__ == '__main__':
    pass
    # tests
