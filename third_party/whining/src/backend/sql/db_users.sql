# Copyright 2016 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

GRANT ALL ON wmdb.* TO 'wmatrix'@'%';
GRANT SELECT ON wmdb.* TO 'wmreader'@'%';
GRANT EXECUTE ON PROCEDURE wmdb.add_triage_comment TO 'wmreader'@'%';

GRANT ALL ON wmdb.* TO 'wmatrix'@'localhost';
GRANT SELECT ON wmdb.* TO 'wmreader'@'localhost';
GRANT EXECUTE ON PROCEDURE wmdb.add_triage_comment TO 'wmreader'@'localhost';

FLUSH PRIVILEGES;
