# Copyright (c) 2013 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""Load suite scheduling info from suite_scheduler.ini

suite_scheduler.ini dictates when each suite is run. It contains a list of
sections similar to the one below:

    [PowerDaily]
    run_on: nightly
    suite: power_daily
    branch_specs: >=R21
    pool: power


 - 'run_on' can be either of weekly, nightly or new_build.
 - 'pool' tells which machines to run on, most tests run on 'suites' pool.

 Rturns:
     A list of dicts. Each dicts corresponds to one section.
"""

import os
import collections
import ConfigParser
import contextlib
import logging
import urllib2

fields = ['run_on', 'suite', 'branch_specs', 'pool']

db_fields = fields + ['rule_name', 'runs_per_week', 'src']
# Note: branch_specs can be a list of one of: RXX like R26, factory, firmware
# look in autotest/files/site_utils/suite_scheduler/taks.py for details

#To avoid exceptions use None defaults
defaults = collections.defaultdict(lambda: None)

runs_per_week = {
    'weekly': 1,
    'nightly': 7,
    'new_build': 7 * 4
    }


def get_schedule(sched_ini, is_url=False):
    cfg = ConfigParser.ConfigParser(defaults=defaults)
    if is_url:
        with contextlib.closing(urllib2.urlopen(sched_ini)) as f:
            cfg.readfp(f, 'suite_scheduler.ini')
    else:
        with open(sched_ini) as f:
            cfg.readfp(f, 'suite_scheduler.ini')
    lst = []
    for section in cfg.sections():
        d = dict((field, cfg.get(section, field)
                 if cfg.has_option(section, field) else None)
                 for field in fields)
        try:
            d['runs_per_week'] = runs_per_week[d['run_on']]
            d['src'] = 'scheduler'
            d['rule_name'] = section
            assert 'suite' in d, '% does not specify suite' % section
            lst.append(d)
        except KeyError as e:
            logging.warning('Cannot process section %s (%s)',
                            section, str(e))
    return lst


if __name__ == '__main__':
    # Tests
    lst = get_schedule('/usr/local/autotest/suite_scheduler.ini')
