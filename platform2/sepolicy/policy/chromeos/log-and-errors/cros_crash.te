# SELinux policy for crash reporting.

type cros_crash, cros_miscdomain, chromeos_domain, domain;

permissive cros_crash;

allow cros_crash chromeos:fd use;

allow cros_crash devpts:chr_file rw_file_perms;
allow cros_crash cros_labeled_dev_type:chr_file rw_file_perms;

allow cros_crash cros_syslog:file r_file_perms;

allow cros_crash unlabeled:dir search;

domain_auto_trans({chromeos_domain -cros_crash}, cros_crash_reporter_exec, cros_crash);
allow cros_crash cros_anomaly_detector:fd use;
# Kernel and cros_anomaly_detector are expected to run crash_reporter.
# This audit allow is used to catch more unexpected usage.
# -chromeos since we're pulling more processes out of chromeos, also it creates noises that crash_test.py
# tries to run crash_reporter during the test.
auditallow {
chromeos_domain
-kernel
-cros_init
-chromeos
-cros_anomaly_detector
-cros_crash
-cros_udevd # Udevd is also expected to execute crash reporter.
-cros_ssh_session
} cros_crash_reporter_exec:file execute;

minijail_netns_new(cros_crash)
log_writer(cros_crash);
cros_dbus_client(cros_crash);
execute_file_follow_link(cros_crash, sh_exec);

allow cros_crash exec_type:file { getattr open read };

allow cros_crash cros_core_collector_exec:file execute;

allow cros_crash { proc_uptime proc_stat }:file r_file_perms;

filetrans_pattern(domain, cros_var_spool, cros_crash_spool, dir, "crash");
auditallow { domain -cros_crash } cros_crash_spool:dir create;

allow cros_crash cros_crash_spool:dir create_dir_perms;
allow cros_crash cros_crash_spool:file create_file_perms;
filetrans_pattern(cros_crash, cros_var_lib, cros_var_lib_crash_reporter, dir, "crash_reporter");
allow cros_crash cros_var_lib_crash_reporter:dir create_dir_perms;
allow cros_crash cros_var_lib_crash_reporter:file create_file_perms;
filetrans_pattern(cros_crash, cros_run, cros_run_crash_reporter, dir, "crash_reporter");
allow cros_crash cros_run_crash_reporter:dir create_dir_perms;
allow cros_crash cros_run_crash_reporter:file create_file_perms;

filetrans_pattern(cros_crash, cros_home_chronos, cros_home_chronos_crash, dir, "crash");
allow cros_crash cros_home_chronos_crash:dir create_dir_perms;
allow cros_crash cros_home_chronos_crash:file create_file_perms;

allow cros_crash kernel:fifo_file { read ioctl };
allow cros_crash domain:dir search;
allow cros_crash domain:file { getattr open read };
allow cros_crash domain:lnk_file read;

# anomaly_detector minijail made it potentially cros_minijail_minijail_tmp_file.
# cros_crash libminijail wants to create on top of it.
tmp_file(cros_crash, dir, , cros_minijail_minijail_tmp_file);
allow cros_crash cros_crash_tmp_file:dir create_dir_perms;
allow cros_crash cros_crash_tmp_file:{file lnk_file} create_file_perms;

r_dir_file(cros_crash, cros_var_lib_whitelist);
r_dir_file(cros_crash, cros_passwd_file);

r_dir_file(cros_crash, cros_anomaly_detector_tmp_file);

r_dir_file(cros_crash, cros_run_containers);

allow cros_crash cros_var_log_eventlog:file r_file_perms;

allow cros_crash self:capability { sys_admin chown sys_nice };
arc_cts_fails_release(`
allow cros_crash self:capability { sys_ptrace dac_override };
', (`cros_crash'));

# neverallow { domain -init } usermodehelper:file { append write };
arc_cts_fails_release(`
allow cros_crash usermodehelper:file w_file_perms;
', (`cros_crash'))

# /proc/sys/kernel/core_pipe_limit in NYC
is_arc_nyc(`
allow cros_crash proc:file w_file_perms;
');

allow cros_crash pstorefs:dir r_dir_perms;
allow cros_crash sysfs_class_devcoredump:file w_file_perms;
allow cros_crash sysfs_class_devcoredump:dir search;

# the following are likely to be minijail related.
# DO NOT PUT non-minijail related policies below.
# tmpfs for /dev is labeled as cros_crash_tmp_file from above.
minijail_mountdev(cros_crash, cros_crash_tmp_file);
minijail_mounts(
 cros_crash,
 ,
 {
  cros_minijail_minijail_tmp_file # created by anomaly_detector minijail
  cros_crash_tmp_file
 }
)

type cros_crash_reporter_tmp_file, cros_tmpfile_type, cros_run_file_type, cros_file_type, file_type;
filetrans_pattern(cros_crash, cros_run, cros_crash_reporter_tmp_file, file, "unclean-shutdown-detected");
filetrans_pattern(cros_crash, cros_run, cros_crash_reporter_tmp_file, file, "kernel-crash-detected");
neverallow { domain -cros_crash -cros_metrics_daemon } cros_crash_tmp_file:file *;
