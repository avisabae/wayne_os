// Copyright 2018 The Chromium OS Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

syntax = "proto3";

option optimize_for = LITE_RUNTIME;

package dlcservice;

// This contains info Update Engine needs to update or install a DLC
// (Downloadable Content) module.
// https://chromium.googlesource.com/chromiumos/platform2/+/HEAD/dlcservice
message DlcModuleInfo {
  // ID of the DLC module.
  // This has to be set.
  // dlc_id is used to construct the DLC module install path and Omaha App ID.
  // Omaha App ID is used to query update from Omaha server. Omaha responds
  // with payload that corresponds to the App ID based on a preset rule.
  // Omaha responds with error if the provided App ID does not exist on
  // Omaha.
  string dlc_id = 1;
}

// This is the message used in:
//
// - The update_engine queries the dlcservice to get the list of available DLCs
//   to be updated (This is done through a D-Bus signal and restricted to
//   update_engine only.) The DLC service constructs the list of available DLCs
//   based on installed DLCs on the stateful partition and returns it to the
//   update_engine.
//
// - The dlcservice sends the list of DLCs it wants to install to the
//   update_engine (similarly, through a D-Bus signal that is restricted to only
//   dlcservice.) Optionally dlcservice can pass a custom Omaha URL (for
//   autotests, QA tests, etc) to download the DLCs from. update_engine checks
//   the validity of this request and starts the installation process.
message DlcModuleList {
  // A list of DLC modules that needs to be installed or updated.
  repeated DlcModuleInfo dlc_module_infos = 1;
  // Omaha URL to query for update. Only used when trying to force the
  // update_engine to download the DLC payloads from a specific address.
  string omaha_url = 2;
}

// This message is sent from DLC Service via OnInstalled signal.
//
// When an install operation finishes, DLC Service sends OnInstalled signal
// with following message.
message InstallResult {
  // Whether install is successful.
  bool success = 1;
  // Error code of the install. See dbus-constants.h for definitions.
  int32 error_code = 2;
  // ID of the installed DLC module.
  string dlc_id = 3;
  // Path to the DLC module content.
  string dlc_root = 4;
}
