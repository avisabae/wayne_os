# Copyright 2019 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

import("//common-mk/mojom_bindings_generator.gni")
import("//common-mk/pkg_config.gni")

group("all") {
  deps = [
    ":cros_healthd",
    ":diag",
    ":smartctl-check",
    ":telem",
    ":urandom",
    ":wilco_dtc_supportd",
  ]
  if (use.test) {
    deps += [
      ":cros_healthd_test",
      ":libcros_healthd_utils_test",
      ":libdiag_test",
      ":libtelem_test",
      ":routine_test",
      ":wilco_dtc_supportd_test",
      "//diagnostics/grpc_async_adapter:libgrpc_async_adapter_test",
    ]
  }
}

# Library with generated Mojo API definitions.
pkg_config("diagnostics_mojo_bindings_pkg_deps") {
  pkg_deps = [
    "libchrome-${libbase_ver}",
    "libmojo-${libbase_ver}",
  ]
}

generate_mojom_bindings("diagnostics_mojo_bindings") {
  public_configs = [ ":diagnostics_mojo_bindings_pkg_deps" ]
  sources = [
    "mojo/cros_healthd.mojom",
    "mojo/cros_healthd_diagnostics.mojom",
    "mojo/cros_healthd_probe.mojom",
    "mojo/cros_healthd_telemetry.mojom",
    "mojo/wilco_dtc_supportd.mojom",
  ]
}

# Library with utilities common to both cros_healthd and wilco_dtc_supportd.
pkg_config("libcommon_pkg_deps") {
  pkg_deps = [ "libchrome-${libbase_ver}" ]
}

static_library("libcommon") {
  public_configs = [ ":libcommon_pkg_deps" ]
  sources = [
    "common/file_test_utils.cc",
  ]
}

# Library that provides the diagnostic routine interface.
pkg_config("libdiag_routine_pkg_deps") {
  pkg_deps = [ "libchrome-${libbase_ver}" ]
}

static_library("libdiag_routine") {
  deps = [
    "//diagnostics/grpc:diagnostics_grpc_protos",
  ]
  all_dependent_configs = [ ":libdiag_routine_pkg_deps" ]
  sources = [
    "routines/battery/battery.cc",
    "routines/battery_sysfs/battery_sysfs.cc",
    "routines/diag_process_adapter_impl.cc",
    "routines/smartctl_check/smartctl_check.cc",
    "routines/subproc_routine.cc",
    "routines/urandom/urandom.cc",
  ]
}

# Library that provides utilities used by cros_healthd.
pkg_config("libcros_healthd_utils_pkg_deps") {
  pkg_deps = [
    "dbus-1",
    "libchrome-${libbase_ver}",
    "libmojo-${libbase_ver}",
    "libudev",
    "protobuf",
    "system_api",
  ]
}

static_library("libcros_healthd_utils") {
  deps = [
    ":diagnostics_mojo_bindings",
  ]
  all_dependent_configs = [ ":libcros_healthd_utils_pkg_deps" ]
  sources = [
    "cros_healthd/utils/battery_utils.cc",
    "cros_healthd/utils/disk_utils.cc",
  ]
}

# Library that provides core functionality for the cros_healthd daemon.
pkg_config("libcros_healthd_pkg_deps") {
  pkg_deps = [
    "dbus-1",
    "libbrillo-${libbase_ver}",
    "libchrome-${libbase_ver}",
    "libmojo-${libbase_ver}",
  ]
}

static_library("libcros_healthd") {
  deps = [
    ":diagnostics_mojo_bindings",
    ":libcros_healthd_utils",
  ]
  all_dependent_configs = [ ":libcros_healthd_pkg_deps" ]
  sources = [
    "cros_healthd/cros_healthd.cc",
    "cros_healthd/cros_healthd_mojo_service.cc",
    "cros_healthd/cros_healthd_routine_service_impl.cc",
    "cros_healthd/cros_healthd_telemetry_service_impl.cc",
  ]
}

# Library that provides core functionality for the diag tool.
pkg_config("libdiag_pkg_deps") {
  pkg_deps = [
    "libbrillo-${libbase_ver}",
    "libchrome-${libbase_ver}",
  ]
}

static_library("libdiag") {
  deps = [
    "//diagnostics/constants:libconstants",
    "//diagnostics/grpc:diagnostics_grpc_protos",
    "//diagnostics/grpc_async_adapter:libgrpc_async_adapter",
  ]
  all_dependent_configs = [ ":libdiag_pkg_deps" ]
  sources = [
    "diag/diag_async_grpc_client_adapter_impl.cc",
    "diag/diag_routine_requester.cc",
  ]
}

# Library that provides core functionality for the wilco_dtc_supportd daemon.
pkg_config("libwilco_dtc_supportd_pkg_deps") {
  pkg_deps = [
    "dbus-1",
    "libbrillo-${libbase_ver}",
    "libchrome-${libbase_ver}",
    "libmojo-${libbase_ver}",
    "system_api",
  ]
}

static_library("libwilco_dtc_supportd") {
  deps = [
    ":diagnostics_mojo_bindings",
    ":libdiag_routine",
    "//diagnostics/constants:libconstants",
    "//diagnostics/grpc:diagnostics_grpc_protos",
    "//diagnostics/grpc_async_adapter:libgrpc_async_adapter",
  ]
  all_dependent_configs = [ ":libwilco_dtc_supportd_pkg_deps" ]
  sources = [
    "wilco_dtc_supportd/ec_constants.cc",
    "wilco_dtc_supportd/json_utils.cc",
    "wilco_dtc_supportd/mojo_utils.cc",
    "wilco_dtc_supportd/vpd_constants.cc",
    "wilco_dtc_supportd/wilco_dtc_supportd_core.cc",
    "wilco_dtc_supportd/wilco_dtc_supportd_core_delegate_impl.cc",
    "wilco_dtc_supportd/wilco_dtc_supportd_daemon.cc",
    "wilco_dtc_supportd/wilco_dtc_supportd_dbus_service.cc",
    "wilco_dtc_supportd/wilco_dtc_supportd_ec_event_service.cc",
    "wilco_dtc_supportd/wilco_dtc_supportd_grpc_service.cc",
    "wilco_dtc_supportd/wilco_dtc_supportd_mojo_service.cc",
    "wilco_dtc_supportd/wilco_dtc_supportd_routine_factory_impl.cc",
    "wilco_dtc_supportd/wilco_dtc_supportd_routine_service.cc",
  ]
}

# Library that provides core functionality for the telemetry tool.
pkg_config("libtelem_pkg_deps") {
  pkg_deps = [
    "libbrillo-${libbase_ver}",
    "libchrome-${libbase_ver}",
    "re2",
    "system_api",
  ]
}

static_library("libtelem") {
  deps = [
    "//diagnostics/constants:libconstants",
    "//diagnostics/grpc:diagnostics_grpc_protos",
    "//diagnostics/grpc_async_adapter:libgrpc_async_adapter",
  ]
  all_dependent_configs = [ ":libtelem_pkg_deps" ]
  sources = [
    "telem/battery_utils.cc",
    "telem/telem_cache.cc",
    "telem/telem_parsers.cc",
    "telem/telemetry.cc",
  ]
}

# The cros_healthd daemon executable.
pkg_config("cros_healthd_pkg_deps") {
  pkg_deps = [ "libbrillo-${libbase_ver}" ]
}

executable("cros_healthd") {
  deps = [
    ":libcros_healthd",
  ]
  sources = [
    "cros_healthd/main.cc",
  ]
}

# The diag tool executable.
pkg_config("diag_pkg_deps") {
  pkg_deps = [
    "libbrillo-${libbase_ver}",
    "libchrome-${libbase_ver}",
  ]
}

executable("diag") {
  deps = [
    ":libdiag",
  ]
  configs += [ ":diag_pkg_deps" ]
  sources = [
    "diag/main.cc",
  ]
}

# The urandom tool executable.
pkg_config("urandom_pkg_deps") {
  pkg_deps = [
    "libbrillo-${libbase_ver}",
    "libchrome-${libbase_ver}",
  ]
}

executable("urandom") {
  configs += [ ":urandom_pkg_deps" ]
  sources = [
    "routines/urandom/main.cc",
  ]
}

# The smartctl-check tool executable.
pkg_config("smartctl-check_pkg_deps") {
  pkg_deps = [
    "libbrillo-${libbase_ver}",
    "libchrome-${libbase_ver}",
    "libdebugd-client",
  ]
}

executable("smartctl-check") {
  configs += [ ":smartctl-check_pkg_deps" ]
  sources = [
    "routines/smartctl_check/main.cc",
    "routines/smartctl_check/smartctl_check_utils.cc",
  ]
}

# The wilco_dtc_supportd daemon executable.
pkg_config("wilco_dtc_supportd_pkg_deps") {
  pkg_deps = [ "libbrillo-${libbase_ver}" ]
}

executable("wilco_dtc_supportd") {
  deps = [
    ":libwilco_dtc_supportd",
  ]
  sources = [
    "wilco_dtc_supportd/main.cc",
  ]
}

# The telemetry tool executable.
pkg_config("telem_pkg_deps") {
  pkg_deps = [
    "libbrillo-${libbase_ver}",
    "libchrome-${libbase_ver}",
  ]
}

executable("telem") {
  deps = [
    ":libtelem",
  ]
  configs += [ ":telem_pkg_deps" ]
  sources = [
    "telem/main.cc",
  ]
}

if (use.test) {
  # Libraries for unit tests.

  # Unit tests.
  pkg_config("cros_healthd_test_pkg_deps") {
    pkg_deps = [
      "libchrome-${libbase_ver}",
      "libchrome-test-${libbase_ver}",
    ]
  }

  executable("cros_healthd_test") {
    configs += [
      ":cros_healthd_test_pkg_deps",
      "//common-mk:test",
    ]
    deps = [
      ":libcros_healthd",
      "../common-mk/testrunner:testrunner",
    ]
    sources = [
      "cros_healthd/cros_healthd_mojo_service_test.cc",
    ]
  }

  pkg_config("libcros_healthd_utils_test_pkg_deps") {
    pkg_deps = [
      "libchrome-${libbase_ver}",
      "libchrome-test-${libbase_ver}",
      "system_api",
    ]
  }

  executable("libcros_healthd_utils_test") {
    configs += [
      ":libcros_healthd_utils_test_pkg_deps",
      "//common-mk:test",
    ]
    deps = [
      ":libcommon",
      ":libcros_healthd_utils",
      "../common-mk/testrunner:testrunner",
    ]
    sources = [
      "cros_healthd/utils/battery_utils_test.cc",
      "cros_healthd/utils/disk_utils_test.cc",
    ]
  }

  pkg_config("wilco_dtc_supportd_test_pkg_deps") {
    pkg_deps = [
      "libchrome-${libbase_ver}",
      "libchrome-test-${libbase_ver}",
    ]
  }

  executable("wilco_dtc_supportd_test") {
    configs += [
      ":wilco_dtc_supportd_test_pkg_deps",
      "//common-mk:test",
    ]
    deps = [
      ":libcommon",
      ":libwilco_dtc_supportd",
      "../common-mk/testrunner:testrunner",
    ]
    sources = [
      "wilco_dtc_supportd/ec_constants_test.cc",
      "wilco_dtc_supportd/fake_browser.cc",
      "wilco_dtc_supportd/fake_wilco_dtc.cc",
      "wilco_dtc_supportd/fake_wilco_dtc_supportd_routine_factory.cc",
      "wilco_dtc_supportd/mock_mojom_wilco_dtc_supportd_client.cc",
      "wilco_dtc_supportd/mojo_test_utils.cc",
      "wilco_dtc_supportd/mojo_utils_test.cc",
      "wilco_dtc_supportd/wilco_dtc_supportd_core_test.cc",
      "wilco_dtc_supportd/wilco_dtc_supportd_dbus_service_test.cc",
      "wilco_dtc_supportd/wilco_dtc_supportd_ec_event_service_test.cc",
      "wilco_dtc_supportd/wilco_dtc_supportd_grpc_service_test.cc",
      "wilco_dtc_supportd/wilco_dtc_supportd_mojo_service_test.cc",
      "wilco_dtc_supportd/wilco_dtc_supportd_routine_service_test.cc",
    ]
  }

  pkg_config("libdiag_test_pkg_deps") {
    pkg_deps = [
      "libchrome-${libbase_ver}",
      "libchrome-test-${libbase_ver}",
    ]
  }

  executable("libdiag_test") {
    configs += [
      "//common-mk:test",
      ":libdiag_test_pkg_deps",
    ]
    deps = [
      ":libdiag",
      "../common-mk/testrunner:testrunner",
    ]
    sources = [
      "diag/diag_routine_requester_test.cc",
    ]
  }

  pkg_config("libtelem_test_pkg_deps") {
    pkg_deps = [
      "libchrome-${libbase_ver}",
      "libchrome-test-${libbase_ver}",
    ]
  }

  executable("libtelem_test") {
    configs += [
      "//common-mk:test",
      ":libtelem_test_pkg_deps",
    ]
    deps = [
      ":libcommon",
      ":libtelem",
      "../common-mk/testrunner:testrunner",
    ]
    sources = [
      "telem/battery_utils_test.cc",
      "telem/cache_writer_impl.cc",
      "telem/telem_cache_test.cc",
      "telem/telem_parsers_test.cc",
      "telem/telemetry_test.cc",
    ]
  }

  pkg_config("routine_test_pkg_deps") {
    pkg_deps = [
      "libchrome-${libbase_ver}",
      "libchrome-test-${libbase_ver}",
    ]
  }

  executable("routine_test") {
    configs += [
      "//common-mk:test",
      ":routine_test_pkg_deps",
    ]
    deps = [
      ":libcommon",
      ":libdiag_routine",
      "../common-mk/testrunner:testrunner",
    ]
    sources = [
      "routines/battery/battery_test.cc",
      "routines/battery_sysfs/battery_sysfs_test.cc",
      "routines/smartctl_check/smartctl_check_utils.cc",
      "routines/smartctl_check/smartctl_check_utils_test.cc",
      "routines/subproc_routine_test.cc",
    ]
  }
}
