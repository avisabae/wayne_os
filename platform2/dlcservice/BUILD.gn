# Copyright 2018 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

import("//common-mk/generate-dbus-adaptors.gni")
import("//common-mk/pkg_config.gni")

group("all") {
  deps = [
    ":dlcservice",
    ":dlcservice_adaptors",
    ":dlcservice_example_install",
    ":dlcservice_example_uninstall",
    ":dlcservice_util",
    ":libdlcservice",
  ]
  if (use.test) {
    deps += [ ":dlcservice_tests" ]
  }
  if (use.fuzzer) {
    deps += [ ":dlcservice_boot_slot_fuzzer" ]
  }
}

pkg_config("target_defaults") {
  pkg_deps = [
    "libchrome-${libbase_ver}",
    "libbrillo-${libbase_ver}",
    "libimageloader-client",
    "libimageloader-manifest",
    "libupdate_engine-client",

    # system_api depends on protobuf (or protobuf-lite). It must appear
    # before protobuf here or the linker flags won't be in the right
    # order.
    "system_api",
    "protobuf-lite",
  ]
}

generate_dbus_adaptors("dlcservice_adaptors") {
  dbus_service_config = "dbus_adaptors/dbus-service-config.json"
  dbus_adaptors_out_dir = "include/dlcservice/dbus_adaptors"
  sources = [
    "dbus_adaptors/org.chromium.DlcServiceInterface.xml",
  ]
}

static_library("libdlcservice") {
  configs += [ ":target_defaults" ]
  libs = [ "rootdev" ]
  sources = [
    "boot_device.cc",
    "boot_device.h",
    "boot_slot.cc",
    "boot_slot.h",
    "dlc_service.cc",
    "dlc_service.h",
    "dlc_service_dbus_adaptor.cc",
    "dlc_service_dbus_adaptor.h",
    "utils.cc",
    "utils.h",
  ]
  deps = [
    ":dlcservice_adaptors",
  ]
}

executable("dlcservice") {
  configs += [ ":target_defaults" ]
  sources = [
    "main.cc",
  ]
  deps = [
    ":libdlcservice",
  ]
}

pkg_config("dlcservice_util_config") {
  pkg_deps = [
    "libdlcservice-client",
    "libminijail",
  ]
}

executable("dlcservice_util") {
  configs += [
    ":target_defaults",
    ":dlcservice_util_config",
  ]
  sources = [
    "tools/dlc_service_util.cc",
  ]
  deps = [
    ":libdlcservice",
  ]
}

executable("dlcservice_example_install") {
  configs += [
    ":target_defaults",
    ":dlcservice_util_config",
  ]
  sources = [
    "examples/install.cc",
  ]
}

executable("dlcservice_example_uninstall") {
  configs += [
    ":target_defaults",
    ":dlcservice_util_config",
  ]
  sources = [
    "examples/uninstall.cc",
  ]
}

if (use.test) {
  pkg_config("dlcservice_tests_config") {
    pkg_deps = [
      "libchrome-test-${libbase_ver}",
      "libimageloader-client-test",
      "libupdate_engine-client-test",
    ]
  }

  executable("dlcservice_tests") {
    configs += [
      "//common-mk:test",
      ":dlcservice_tests_config",
      ":target_defaults",
    ]
    sources = [
      "boot_device_test.cc",
      "boot_slot_test.cc",
      "dlc_service_dbus_adaptor_test.cc",
      "utils_test.cc",
    ]
    deps = [
      ":libdlcservice",
      "//common-mk/testrunner",
    ]
  }
}

if (use.fuzzer) {
  pkg_config("dlcservice_fuzzer_config") {
    pkg_deps = [ "libchrome-test-${libbase_ver}" ]
  }
  executable("dlcservice_boot_slot_fuzzer") {
    configs += [
      "//common-mk/common_fuzzer",
      ":dlcservice_fuzzer_config",
      ":target_defaults",
    ]
    sources = [
      "boot_slot_fuzzer.cc",
    ]
    deps = [
      ":libdlcservice",
    ]
  }
}
