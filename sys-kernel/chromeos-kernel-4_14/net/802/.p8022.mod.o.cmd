cmd_net/802/p8022.mod.o := x86_64-pc-linux-gnu-clang -B/usr/x86_64-pc-linux-gnu/binutils-bin/2.27.0 -Wp,-MD,net/802/.p8022.mod.o.d  -nostdinc -isystem /usr/lib64/clang/9.0.0/include -I/var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include -I./arch/x86/include/generated  -I/var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include -I./include -I/var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/uapi -I./arch/x86/include/generated/uapi -I/var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi -I./include/generated/uapi -include /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/kconfig.h  -D__KERNEL__ -Qunused-arguments -Wall -Wundef -Wstrict-prototypes -Wno-trigraphs -fno-strict-aliasing -fno-common -fshort-wchar -Werror-implicit-function-declaration -Wno-format-security -std=gnu89 --target=x86_64-pc-linux-gnu --prefix=/usr/bin/ --gcc-toolchain=/usr -no-integrated-as -fno-PIE -mno-sse -mno-mmx -mno-sse2 -mno-3dnow -mno-avx -m64 -mno-80387 -mstack-alignment=8 -mtune=generic -mno-red-zone -mcmodel=kernel -funit-at-a-time -DCONFIG_AS_CFI=1 -DCONFIG_AS_CFI_SIGNAL_FRAME=1 -DCONFIG_AS_CFI_SECTIONS=1 -DCONFIG_AS_FXSAVEQ=1 -DCONFIG_AS_SSSE3=1 -DCONFIG_AS_CRC32=1 -DCONFIG_AS_AVX=1 -DCONFIG_AS_AVX2=1 -DCONFIG_AS_AVX512=1 -DCONFIG_AS_SHA1_NI=1 -DCONFIG_AS_SHA256_NI=1 -pipe -Wno-sign-compare -fno-asynchronous-unwind-tables -mretpoline-external-thunk -fno-jump-tables -fno-delete-null-pointer-checks -Wno-frame-address -Wno-format-truncation -Wno-format-overflow -Wno-int-in-bool-context -Wno-attribute-alias -Os -Wno-maybe-uninitialized -Wno-maybe-uninitialized --param=allow-store-data-races=0 -Wframe-larger-than=2048 -fstack-protector-strong -Wno-format-invalid-specifier -Wno-gnu -Wno-address-of-packed-member -Wno-duplicate-decl-specifier -Wno-tautological-compare -Wno-constant-conversion -mno-global-merge -Wno-unused-const-variable -g -pg -mfentry -DCC_USING_FENTRY -Werror -Wdeclaration-after-statement -Wno-pointer-sign -Wno-stringop-truncation -fno-strict-overflow -fno-merge-all-constants -fno-stack-check -Werror=implicit-int -Werror=strict-prototypes -Werror=date-time -Werror=incompatible-pointer-types -Werror=designated-init -Wno-packed-not-aligned -Wno-initializer-overrides -Wno-unused-value -Wno-format -Wno-sign-compare -Wno-format-zero-length -Wno-uninitialized -fdebug-info-for-profiling  -DKBUILD_BASENAME='"p8022.mod"'  -DKBUILD_MODNAME='"p8022"' -DMODULE  -c -o net/802/p8022.mod.o net/802/p8022.mod.c

source_net/802/p8022.mod.o := net/802/p8022.mod.c

deps_net/802/p8022.mod.o := \
    $(wildcard include/config/retpoline.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/compiler_types.h \
    $(wildcard include/config/have/arch/compiler/h.h) \
    $(wildcard include/config/enable/must/check.h) \
    $(wildcard include/config/enable/warn/deprecated.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/compiler-gcc.h \
    $(wildcard include/config/arch/supports/optimized/inlining.h) \
    $(wildcard include/config/optimize/inlining.h) \
    $(wildcard include/config/gcov/kernel.h) \
    $(wildcard include/config/arch/use/builtin/bswap.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/compiler-clang.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/module.h \
    $(wildcard include/config/modules.h) \
    $(wildcard include/config/sysfs.h) \
    $(wildcard include/config/modules/tree/lookup.h) \
    $(wildcard include/config/livepatch.h) \
    $(wildcard include/config/unused/symbols.h) \
    $(wildcard include/config/module/sig.h) \
    $(wildcard include/config/generic/bug.h) \
    $(wildcard include/config/kallsyms.h) \
    $(wildcard include/config/smp.h) \
    $(wildcard include/config/tracepoints.h) \
    $(wildcard include/config/tracing.h) \
    $(wildcard include/config/event/tracing.h) \
    $(wildcard include/config/ftrace/mcount/record.h) \
    $(wildcard include/config/module/unload.h) \
    $(wildcard include/config/constructors.h) \
    $(wildcard include/config/strict/module/rwx.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/list.h \
    $(wildcard include/config/debug/list.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/types.h \
    $(wildcard include/config/have/uid16.h) \
    $(wildcard include/config/uid16.h) \
    $(wildcard include/config/lbdaf.h) \
    $(wildcard include/config/arch/dma/addr/t/64bit.h) \
    $(wildcard include/config/phys/addr/t/64bit.h) \
    $(wildcard include/config/64bit.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/types.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/uapi/asm/types.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/asm-generic/types.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/asm-generic/int-ll64.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/asm-generic/int-ll64.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/uapi/asm/bitsperlong.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/asm-generic/bitsperlong.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/asm-generic/bitsperlong.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/posix_types.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/stddef.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/stddef.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/posix_types.h \
    $(wildcard include/config/x86/32.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/uapi/asm/posix_types_64.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/asm-generic/posix_types.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/poison.h \
    $(wildcard include/config/illegal/pointer/value.h) \
    $(wildcard include/config/page/poisoning/zero.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/const.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/kernel.h \
    $(wildcard include/config/preempt/voluntary.h) \
    $(wildcard include/config/debug/atomic/sleep.h) \
    $(wildcard include/config/mmu.h) \
    $(wildcard include/config/prove/locking.h) \
    $(wildcard include/config/arch/has/refcount.h) \
    $(wildcard include/config/panic/timeout.h) \
  /usr/lib64/clang/9.0.0/include/stdarg.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/linkage.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/stringify.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/export.h \
    $(wildcard include/config/have/underscore/symbol/prefix.h) \
    $(wildcard include/config/modversions.h) \
    $(wildcard include/config/module/rel/crcs.h) \
    $(wildcard include/config/trim/unused/ksyms.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/linkage.h \
    $(wildcard include/config/x86/64.h) \
    $(wildcard include/config/x86/alignment/16.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/compiler.h \
    $(wildcard include/config/trace/branch/profiling.h) \
    $(wildcard include/config/profile/all/branches.h) \
    $(wildcard include/config/stack/validation.h) \
    $(wildcard include/config/kasan.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/barrier.h \
    $(wildcard include/config/x86/ppro/fence.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/alternative.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/asm.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/nops.h \
    $(wildcard include/config/mk7.h) \
    $(wildcard include/config/x86/p6/nop.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/asm-generic/barrier.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/bitops.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/bits.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/bitops.h \
    $(wildcard include/config/x86/cmov.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/rmwcc.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/asm-generic/bitops/find.h \
    $(wildcard include/config/generic/find/first/bit.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/asm-generic/bitops/sched.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/arch_hweight.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/cpufeatures.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/required-features.h \
    $(wildcard include/config/x86/minimum/cpu/family.h) \
    $(wildcard include/config/math/emulation.h) \
    $(wildcard include/config/x86/pae.h) \
    $(wildcard include/config/x86/cmpxchg64.h) \
    $(wildcard include/config/x86/use/3dnow.h) \
    $(wildcard include/config/matom.h) \
    $(wildcard include/config/x86/5level.h) \
    $(wildcard include/config/paravirt.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/disabled-features.h \
    $(wildcard include/config/x86/intel/mpx.h) \
    $(wildcard include/config/x86/intel/memory/protection/keys.h) \
    $(wildcard include/config/page/table/isolation.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/asm-generic/bitops/const_hweight.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/asm-generic/bitops/le.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/uapi/asm/byteorder.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/byteorder/little_endian.h \
    $(wildcard include/config/cpu/big/endian.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/byteorder/little_endian.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/swab.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/swab.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/uapi/asm/swab.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/byteorder/generic.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/asm-generic/bitops/ext2-atomic-setbit.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/log2.h \
    $(wildcard include/config/arch/has/ilog2/u32.h) \
    $(wildcard include/config/arch/has/ilog2/u64.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/typecheck.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/printk.h \
    $(wildcard include/config/message/loglevel/default.h) \
    $(wildcard include/config/console/loglevel/default.h) \
    $(wildcard include/config/early/printk.h) \
    $(wildcard include/config/printk/nmi.h) \
    $(wildcard include/config/printk.h) \
    $(wildcard include/config/dynamic/debug.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/init.h \
    $(wildcard include/config/strict/kernel/rwx.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/kern_levels.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/cache.h \
    $(wildcard include/config/arch/has/cache/line/size.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/kernel.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/sysinfo.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/cache.h \
    $(wildcard include/config/x86/l1/cache/shift.h) \
    $(wildcard include/config/x86/internode/cache/shift.h) \
    $(wildcard include/config/x86/vsmp.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/build_bug.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/stat.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/uapi/asm/stat.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/stat.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/time.h \
    $(wildcard include/config/arch/uses/gettimeoffset.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/seqlock.h \
    $(wildcard include/config/debug/lock/alloc.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/spinlock.h \
    $(wildcard include/config/debug/spinlock.h) \
    $(wildcard include/config/generic/lockbreak.h) \
    $(wildcard include/config/preempt.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/preempt.h \
    $(wildcard include/config/preempt/count.h) \
    $(wildcard include/config/debug/preempt.h) \
    $(wildcard include/config/preempt/tracer.h) \
    $(wildcard include/config/preempt/notifiers.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/preempt.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/percpu.h \
    $(wildcard include/config/x86/64/smp.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/asm-generic/percpu.h \
    $(wildcard include/config/have/setup/per/cpu/area.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/threads.h \
    $(wildcard include/config/nr/cpus.h) \
    $(wildcard include/config/base/small.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/percpu-defs.h \
    $(wildcard include/config/debug/force/weak/per/cpu.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/thread_info.h \
    $(wildcard include/config/thread/info/in/task.h) \
    $(wildcard include/config/have/arch/within/stack/frames.h) \
    $(wildcard include/config/hardened/usercopy.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/bug.h \
    $(wildcard include/config/bug/on/data/corruption.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/bug.h \
    $(wildcard include/config/debug/bugverbose.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/asm-generic/bug.h \
    $(wildcard include/config/bug.h) \
    $(wildcard include/config/generic/bug/relative/pointers.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/restart_block.h \
    $(wildcard include/config/compat.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/current.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/thread_info.h \
    $(wildcard include/config/vm86.h) \
    $(wildcard include/config/alt/syscall.h) \
    $(wildcard include/config/ia32/emulation.h) \
    $(wildcard include/config/frame/pointer.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/page.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/page_types.h \
    $(wildcard include/config/physical/start.h) \
    $(wildcard include/config/physical/align.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/mem_encrypt.h \
    $(wildcard include/config/arch/has/mem/encrypt.h) \
    $(wildcard include/config/amd/mem/encrypt.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/mem_encrypt.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/uapi/asm/bootparam.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/screen_info.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/screen_info.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/apm_bios.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/apm_bios.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/ioctl.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/uapi/asm/ioctl.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/asm-generic/ioctl.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/asm-generic/ioctl.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/edd.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/edd.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/ist.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/uapi/asm/ist.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/video/edid.h \
    $(wildcard include/config/x86.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/video/edid.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/page_64_types.h \
    $(wildcard include/config/kasan/extra.h) \
    $(wildcard include/config/randomize/memory.h) \
    $(wildcard include/config/randomize/base.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/kaslr.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/page_64.h \
    $(wildcard include/config/debug/virtual.h) \
    $(wildcard include/config/flatmem.h) \
    $(wildcard include/config/x86/vsyscall/emulation.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/range.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/asm-generic/memory_model.h \
    $(wildcard include/config/discontigmem.h) \
    $(wildcard include/config/sparsemem/vmemmap.h) \
    $(wildcard include/config/sparsemem.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/pfn.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/asm-generic/getorder.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/cpufeature.h \
    $(wildcard include/config/x86/feature/names.h) \
    $(wildcard include/config/x86/fast/feature/tests.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/processor.h \
    $(wildcard include/config/cc/stackprotector.h) \
    $(wildcard include/config/x86/debugctlmsr.h) \
    $(wildcard include/config/cpu/sup/amd.h) \
    $(wildcard include/config/xen.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/processor-flags.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/uapi/asm/processor-flags.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/math_emu.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/ptrace.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/segment.h \
    $(wildcard include/config/xen/pv.h) \
    $(wildcard include/config/x86/32/lazy/gs.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/uapi/asm/ptrace.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/uapi/asm/ptrace-abi.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/asm-generic/ptrace.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/uapi/asm/sigcontext.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/pgtable_types.h \
    $(wildcard include/config/mem/soft/dirty.h) \
    $(wildcard include/config/pgtable/levels.h) \
    $(wildcard include/config/proc/fs.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/pgtable_64_types.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/sparsemem.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/asm-generic/pgtable-nop4d.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/msr.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/msr-index.h \
    $(wildcard include/config/control.h) \
    $(wildcard include/config/tdp/nominal.h) \
    $(wildcard include/config/tdp/level/1.h) \
    $(wildcard include/config/tdp/level/2.h) \
    $(wildcard include/config/tdp/control.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/uapi/asm/errno.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/asm-generic/errno.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/asm-generic/errno-base.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/cpumask.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/cpumask.h \
    $(wildcard include/config/cpumask/offstack.h) \
    $(wildcard include/config/hotplug/cpu.h) \
    $(wildcard include/config/debug/per/cpu/maps.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/bitmap.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/string.h \
    $(wildcard include/config/binary/printf.h) \
    $(wildcard include/config/fortify/source.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/string.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/string.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/string_64.h \
    $(wildcard include/config/x86/mce.h) \
    $(wildcard include/config/arch/has/uaccess/flushcache.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/jump_label.h \
    $(wildcard include/config/jump/label.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/atomic.h \
    $(wildcard include/config/generic/atomic64.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/atomic.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/cmpxchg.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/cmpxchg_64.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/atomic64_64.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/asm-generic/atomic-long.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/uapi/asm/msr.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/tracepoint-defs.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/static_key.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/errno.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/errno.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/desc_defs.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/special_insns.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/fpu/types.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/unwind_hints.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/orc_types.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/personality.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/personality.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/math64.h \
    $(wildcard include/config/arch/supports/int128.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/div64.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/asm-generic/div64.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/err.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/irqflags.h \
    $(wildcard include/config/trace/irqflags.h) \
    $(wildcard include/config/irqsoff/tracer.h) \
    $(wildcard include/config/trace/irqflags/support.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/irqflags.h \
    $(wildcard include/config/debug/entry.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/nospec-branch.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/alternative-asm.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/bottom_half.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/spinlock_types.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/spinlock_types.h \
    $(wildcard include/config/paravirt/spinlocks.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/asm-generic/qspinlock_types.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/asm-generic/qrwlock_types.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/lockdep.h \
    $(wildcard include/config/lockdep.h) \
    $(wildcard include/config/lock/stat.h) \
    $(wildcard include/config/lockdep/crossrelease.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/rwlock_types.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/spinlock.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/paravirt.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/qspinlock.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/asm-generic/qspinlock.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/qrwlock.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/asm-generic/qrwlock.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/rwlock.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/spinlock_api_smp.h \
    $(wildcard include/config/inline/spin/lock.h) \
    $(wildcard include/config/inline/spin/lock/bh.h) \
    $(wildcard include/config/inline/spin/lock/irq.h) \
    $(wildcard include/config/inline/spin/lock/irqsave.h) \
    $(wildcard include/config/inline/spin/trylock.h) \
    $(wildcard include/config/inline/spin/trylock/bh.h) \
    $(wildcard include/config/uninline/spin/unlock.h) \
    $(wildcard include/config/inline/spin/unlock/bh.h) \
    $(wildcard include/config/inline/spin/unlock/irq.h) \
    $(wildcard include/config/inline/spin/unlock/irqrestore.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/rwlock_api_smp.h \
    $(wildcard include/config/inline/read/lock.h) \
    $(wildcard include/config/inline/write/lock.h) \
    $(wildcard include/config/inline/read/lock/bh.h) \
    $(wildcard include/config/inline/write/lock/bh.h) \
    $(wildcard include/config/inline/read/lock/irq.h) \
    $(wildcard include/config/inline/write/lock/irq.h) \
    $(wildcard include/config/inline/read/lock/irqsave.h) \
    $(wildcard include/config/inline/write/lock/irqsave.h) \
    $(wildcard include/config/inline/read/trylock.h) \
    $(wildcard include/config/inline/write/trylock.h) \
    $(wildcard include/config/inline/read/unlock.h) \
    $(wildcard include/config/inline/write/unlock.h) \
    $(wildcard include/config/inline/read/unlock/bh.h) \
    $(wildcard include/config/inline/write/unlock/bh.h) \
    $(wildcard include/config/inline/read/unlock/irq.h) \
    $(wildcard include/config/inline/write/unlock/irq.h) \
    $(wildcard include/config/inline/read/unlock/irqrestore.h) \
    $(wildcard include/config/inline/write/unlock/irqrestore.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/time64.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/time.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/uidgid.h \
    $(wildcard include/config/multiuser.h) \
    $(wildcard include/config/user/ns.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/highuid.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/kmod.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/umh.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/gfp.h \
    $(wildcard include/config/highmem.h) \
    $(wildcard include/config/zone/dma.h) \
    $(wildcard include/config/zone/dma32.h) \
    $(wildcard include/config/zone/device.h) \
    $(wildcard include/config/numa.h) \
    $(wildcard include/config/pm/sleep.h) \
    $(wildcard include/config/memory/isolation.h) \
    $(wildcard include/config/compaction.h) \
    $(wildcard include/config/cma.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/mmdebug.h \
    $(wildcard include/config/debug/vm.h) \
    $(wildcard include/config/debug/vm/pgflags.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/mmzone.h \
    $(wildcard include/config/force/max/zoneorder.h) \
    $(wildcard include/config/zsmalloc.h) \
    $(wildcard include/config/memcg.h) \
    $(wildcard include/config/memory/hotplug.h) \
    $(wildcard include/config/flat/node/mem/map.h) \
    $(wildcard include/config/page/extension.h) \
    $(wildcard include/config/no/bootmem.h) \
    $(wildcard include/config/numa/balancing.h) \
    $(wildcard include/config/deferred/struct/page/init.h) \
    $(wildcard include/config/transparent/hugepage.h) \
    $(wildcard include/config/have/memory/present.h) \
    $(wildcard include/config/have/memoryless/nodes.h) \
    $(wildcard include/config/need/node/memmap/size.h) \
    $(wildcard include/config/have/memblock/node/map.h) \
    $(wildcard include/config/need/multiple/nodes.h) \
    $(wildcard include/config/have/arch/early/pfn/to/nid.h) \
    $(wildcard include/config/sparsemem/extreme.h) \
    $(wildcard include/config/memory/hotremove.h) \
    $(wildcard include/config/have/arch/pfn/valid.h) \
    $(wildcard include/config/holes/in/zone.h) \
    $(wildcard include/config/arch/has/holes/memorymodel.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/wait.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/wait.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/numa.h \
    $(wildcard include/config/nodes/shift.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/nodemask.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/pageblock-flags.h \
    $(wildcard include/config/hugetlb/page.h) \
    $(wildcard include/config/hugetlb/page/size/variable.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/page-flags-layout.h \
  include/generated/bounds.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/memory_hotplug.h \
    $(wildcard include/config/arch/has/add/pages.h) \
    $(wildcard include/config/have/arch/nodedata/extension.h) \
    $(wildcard include/config/have/bootmem/info/node.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/notifier.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/mutex.h \
    $(wildcard include/config/mutex/spin/on/owner.h) \
    $(wildcard include/config/debug/mutexes.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/osq_lock.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/debug_locks.h \
    $(wildcard include/config/debug/locking/api/selftests.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/rwsem.h \
    $(wildcard include/config/rwsem/spin/on/owner.h) \
    $(wildcard include/config/rwsem/generic/spinlock.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/rwsem.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/srcu.h \
    $(wildcard include/config/tiny/srcu.h) \
    $(wildcard include/config/tree/srcu.h) \
    $(wildcard include/config/srcu.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/rcupdate.h \
    $(wildcard include/config/preempt/rcu.h) \
    $(wildcard include/config/rcu/stall/common.h) \
    $(wildcard include/config/no/hz/full.h) \
    $(wildcard include/config/rcu/nocb/cpu.h) \
    $(wildcard include/config/tasks/rcu.h) \
    $(wildcard include/config/tree/rcu.h) \
    $(wildcard include/config/tiny/rcu.h) \
    $(wildcard include/config/debug/objects/rcu/head.h) \
    $(wildcard include/config/prove/rcu.h) \
    $(wildcard include/config/rcu/boost.h) \
    $(wildcard include/config/arch/weak/release/acquire.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/rcutree.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/workqueue.h \
    $(wildcard include/config/debug/objects/work.h) \
    $(wildcard include/config/freezer.h) \
    $(wildcard include/config/wq/watchdog.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/timer.h \
    $(wildcard include/config/debug/objects/timers.h) \
    $(wildcard include/config/no/hz/common.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/ktime.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/jiffies.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/timex.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/timex.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/param.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/uapi/asm/param.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/asm-generic/param.h \
    $(wildcard include/config/hz.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/asm-generic/param.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/timex.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/tsc.h \
    $(wildcard include/config/x86/tsc.h) \
  include/generated/timeconst.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/timekeeping.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/debugobjects.h \
    $(wildcard include/config/debug/objects.h) \
    $(wildcard include/config/debug/objects/free.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/rcu_segcblist.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/srcutree.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/rcu_node_tree.h \
    $(wildcard include/config/rcu/fanout.h) \
    $(wildcard include/config/rcu/fanout/leaf.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/completion.h \
    $(wildcard include/config/lockdep/completions.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/topology.h \
    $(wildcard include/config/use/percpu/numa/node/id.h) \
    $(wildcard include/config/sched/smt.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/smp.h \
    $(wildcard include/config/up/late/init.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/llist.h \
    $(wildcard include/config/arch/have/nmi/safe/cmpxchg.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/smp.h \
    $(wildcard include/config/x86/local/apic.h) \
    $(wildcard include/config/x86/io/apic.h) \
    $(wildcard include/config/debug/nmi/selftest.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/mpspec.h \
    $(wildcard include/config/eisa.h) \
    $(wildcard include/config/x86/mpparse.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/mpspec_def.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/x86_init.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/apicdef.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/apic.h \
    $(wildcard include/config/x86/x2apic.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/fixmap.h \
    $(wildcard include/config/provide/ohci1394/dma/init.h) \
    $(wildcard include/config/pci/mmconfig.h) \
    $(wildcard include/config/x86/intel/mid.h) \
    $(wildcard include/config/acpi/apei/ghes.h) \
    $(wildcard include/config/intel/txt.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/acpi.h \
    $(wildcard include/config/acpi/apei.h) \
    $(wildcard include/config/acpi.h) \
    $(wildcard include/config/acpi/numa.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/acpi/pdc_intel.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/numa.h \
    $(wildcard include/config/numa/emu.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/topology.h \
    $(wildcard include/config/sched/mc/prio.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/asm-generic/topology.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/mmu.h \
    $(wildcard include/config/modify/ldt/syscall.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/realmode.h \
    $(wildcard include/config/acpi/sleep.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/io.h \
    $(wildcard include/config/mtrr.h) \
    $(wildcard include/config/x86/pat.h) \
  arch/x86/include/generated/asm/early_ioremap.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/asm-generic/early_ioremap.h \
    $(wildcard include/config/generic/early/ioremap.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/asm-generic/iomap.h \
    $(wildcard include/config/has/ioport/map.h) \
    $(wildcard include/config/pci.h) \
    $(wildcard include/config/generic/iomap.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/asm-generic/pci_iomap.h \
    $(wildcard include/config/no/generic/pci/ioport/map.h) \
    $(wildcard include/config/generic/pci/iomap.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/asm-generic/io.h \
    $(wildcard include/config/virt/to/bus.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/vmalloc.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/rbtree.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/uapi/asm/vsyscall.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/asm-generic/fixmap.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/hardirq.h \
    $(wildcard include/config/kvm/intel.h) \
    $(wildcard include/config/have/kvm.h) \
    $(wildcard include/config/x86/thermal/vector.h) \
    $(wildcard include/config/x86/mce/threshold.h) \
    $(wildcard include/config/x86/mce/amd.h) \
    $(wildcard include/config/hyperv.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/io_apic.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/irq_vectors.h \
    $(wildcard include/config/pci/msi.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/percpu.h \
    $(wildcard include/config/need/per/cpu/embed/first/chunk.h) \
    $(wildcard include/config/need/per/cpu/page/first/chunk.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/sysctl.h \
    $(wildcard include/config/sysctl.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/sysctl.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/elf.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/elf.h \
    $(wildcard include/config/x86/x32/abi.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/user.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/user_64.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/uapi/asm/auxvec.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/vdso.h \
    $(wildcard include/config/x86/x32.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/mm_types.h \
    $(wildcard include/config/have/cmpxchg/double.h) \
    $(wildcard include/config/have/aligned/struct/page.h) \
    $(wildcard include/config/userfaultfd.h) \
    $(wildcard include/config/have/arch/compat/mmap/bases.h) \
    $(wildcard include/config/membarrier.h) \
    $(wildcard include/config/aio.h) \
    $(wildcard include/config/mmu/notifier.h) \
    $(wildcard include/config/arch/want/batched/unmap/tlb/flush.h) \
    $(wildcard include/config/hmm.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/mm_types_task.h \
    $(wildcard include/config/split/ptlock/cpus.h) \
    $(wildcard include/config/arch/enable/split/pmd/ptlock.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/tlbbatch.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/auxvec.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/auxvec.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/uprobes.h \
    $(wildcard include/config/uprobes.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/uprobes.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/elf.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/elf-em.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/kobject.h \
    $(wildcard include/config/uevent/helper.h) \
    $(wildcard include/config/debug/kobject/release.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/sysfs.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/kernfs.h \
    $(wildcard include/config/kernfs.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/idr.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/radix-tree.h \
    $(wildcard include/config/radix/tree/multiorder.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/kobject_ns.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/kref.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/refcount.h \
    $(wildcard include/config/refcount/full.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/refcount.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/moduleparam.h \
    $(wildcard include/config/alpha.h) \
    $(wildcard include/config/ia64.h) \
    $(wildcard include/config/ppc64.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/rbtree_latch.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/module.h \
    $(wildcard include/config/unwinder/orc.h) \
    $(wildcard include/config/m486.h) \
    $(wildcard include/config/m586.h) \
    $(wildcard include/config/m586tsc.h) \
    $(wildcard include/config/m586mmx.h) \
    $(wildcard include/config/mcore2.h) \
    $(wildcard include/config/m686.h) \
    $(wildcard include/config/mpentiumii.h) \
    $(wildcard include/config/mpentiumiii.h) \
    $(wildcard include/config/mpentiumm.h) \
    $(wildcard include/config/mpentium4.h) \
    $(wildcard include/config/mk6.h) \
    $(wildcard include/config/mk8.h) \
    $(wildcard include/config/melan.h) \
    $(wildcard include/config/mcrusoe.h) \
    $(wildcard include/config/mefficeon.h) \
    $(wildcard include/config/mwinchipc6.h) \
    $(wildcard include/config/mwinchip3d.h) \
    $(wildcard include/config/mcyrixiii.h) \
    $(wildcard include/config/mviac3/2.h) \
    $(wildcard include/config/mviac7.h) \
    $(wildcard include/config/mgeodegx1.h) \
    $(wildcard include/config/mgeode/lx.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/asm-generic/module.h \
    $(wildcard include/config/have/mod/arch/specific.h) \
    $(wildcard include/config/modules/use/elf/rel.h) \
    $(wildcard include/config/modules/use/elf/rela.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/vermagic.h \
  include/generated/utsrelease.h \

net/802/p8022.mod.o: $(deps_net/802/p8022.mod.o)

$(deps_net/802/p8022.mod.o):
