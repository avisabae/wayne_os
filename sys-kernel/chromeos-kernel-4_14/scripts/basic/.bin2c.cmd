cmd_scripts/basic/bin2c := x86_64-pc-linux-gnu-clang -Wp,-MD,scripts/basic/.bin2c.d -Iscripts/basic -Wall -Wmissing-prototypes -Wstrict-prototypes -O2 -fomit-frame-pointer -std=gnu89 -o scripts/basic/bin2c /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/scripts/basic/bin2c.c  

source_scripts/basic/bin2c := /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/scripts/basic/bin2c.c

deps_scripts/basic/bin2c := \
  /usr/include/stdio.h \
  /usr/include/bits/libc-header-start.h \
  /usr/include/features.h \
  /usr/include/stdc-predef.h \
  /usr/include/sys/cdefs.h \
  /usr/include/bits/wordsize.h \
  /usr/include/bits/long-double.h \
  /usr/include/gnu/stubs.h \
  /usr/include/gnu/stubs-64.h \
  /usr/lib64/clang/9.0.0/include/stddef.h \
  /usr/include/bits/types.h \
  /usr/include/bits/typesizes.h \
  /usr/include/bits/types/__FILE.h \
  /usr/include/bits/types/FILE.h \
  /usr/include/bits/libio.h \
  /usr/include/bits/_G_config.h \
    $(wildcard include/config/h.h) \
  /usr/include/bits/types/__mbstate_t.h \
  /usr/lib64/clang/9.0.0/include/stdarg.h \
  /usr/include/bits/stdio_lim.h \
  /usr/include/bits/sys_errlist.h \
  /usr/include/bits/stdio.h \

scripts/basic/bin2c: $(deps_scripts/basic/bin2c)

$(deps_scripts/basic/bin2c):
