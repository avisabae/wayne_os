cmd_scripts/mod/devicetable-offsets.s := x86_64-pc-linux-gnu-clang -B/usr/x86_64-pc-linux-gnu/binutils-bin/2.27.0 -Wp,-MD,scripts/mod/.devicetable-offsets.s.d  -nostdinc -isystem /usr/lib64/clang/9.0.0/include -I/var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include -I./arch/x86/include/generated  -I/var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include -I./include -I/var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/uapi -I./arch/x86/include/generated/uapi -I/var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi -I./include/generated/uapi -include /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/kconfig.h  -I/var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/scripts/mod -Iscripts/mod -D__KERNEL__ -Qunused-arguments -Wall -Wundef -Wstrict-prototypes -Wno-trigraphs -fno-strict-aliasing -fno-common -fshort-wchar -Werror-implicit-function-declaration -Wno-format-security -std=gnu89 --target=x86_64-pc-linux-gnu --prefix=/usr/bin/ --gcc-toolchain=/usr -no-integrated-as -fno-PIE -mno-sse -mno-mmx -mno-sse2 -mno-3dnow -mno-avx -m64 -mno-80387 -mstack-alignment=8 -mtune=generic -mno-red-zone -mcmodel=kernel -funit-at-a-time -DCONFIG_AS_CFI=1 -DCONFIG_AS_CFI_SIGNAL_FRAME=1 -DCONFIG_AS_CFI_SECTIONS=1 -DCONFIG_AS_FXSAVEQ=1 -DCONFIG_AS_SSSE3=1 -DCONFIG_AS_CRC32=1 -DCONFIG_AS_AVX=1 -DCONFIG_AS_AVX2=1 -DCONFIG_AS_AVX512=1 -DCONFIG_AS_SHA1_NI=1 -DCONFIG_AS_SHA256_NI=1 -pipe -Wno-sign-compare -fno-asynchronous-unwind-tables -mretpoline-external-thunk -fno-jump-tables -fno-delete-null-pointer-checks -Wno-frame-address -Wno-format-truncation -Wno-format-overflow -Wno-int-in-bool-context -Wno-attribute-alias -Os -Wno-maybe-uninitialized -Wno-maybe-uninitialized --param=allow-store-data-races=0 -Wframe-larger-than=2048 -fstack-protector-strong -Wno-format-invalid-specifier -Wno-gnu -Wno-address-of-packed-member -Wno-duplicate-decl-specifier -Wno-tautological-compare -Wno-constant-conversion -mno-global-merge -Wno-unused-const-variable -g -pg -mfentry -DCC_USING_FENTRY -Werror -Wdeclaration-after-statement -Wno-pointer-sign -Wno-stringop-truncation -fno-strict-overflow -fno-merge-all-constants -fno-stack-check -Werror=implicit-int -Werror=strict-prototypes -Werror=date-time -Werror=incompatible-pointer-types -Werror=designated-init -Wno-packed-not-aligned -Wno-initializer-overrides -Wno-unused-value -Wno-format -Wno-sign-compare -Wno-format-zero-length -Wno-uninitialized -fdebug-info-for-profiling    -DKBUILD_BASENAME='"devicetable_offsets"'  -DKBUILD_MODNAME='"devicetable_offsets"'  -fverbose-asm -S -o scripts/mod/devicetable-offsets.s /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/scripts/mod/devicetable-offsets.c

source_scripts/mod/devicetable-offsets.s := /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/scripts/mod/devicetable-offsets.c

deps_scripts/mod/devicetable-offsets.s := \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/compiler_types.h \
    $(wildcard include/config/have/arch/compiler/h.h) \
    $(wildcard include/config/enable/must/check.h) \
    $(wildcard include/config/enable/warn/deprecated.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/compiler-gcc.h \
    $(wildcard include/config/arch/supports/optimized/inlining.h) \
    $(wildcard include/config/optimize/inlining.h) \
    $(wildcard include/config/retpoline.h) \
    $(wildcard include/config/gcov/kernel.h) \
    $(wildcard include/config/arch/use/builtin/bswap.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/compiler-clang.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/kbuild.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/mod_devicetable.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/types.h \
    $(wildcard include/config/have/uid16.h) \
    $(wildcard include/config/uid16.h) \
    $(wildcard include/config/lbdaf.h) \
    $(wildcard include/config/arch/dma/addr/t/64bit.h) \
    $(wildcard include/config/phys/addr/t/64bit.h) \
    $(wildcard include/config/64bit.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/types.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/uapi/asm/types.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/asm-generic/types.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/asm-generic/int-ll64.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/asm-generic/int-ll64.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/uapi/asm/bitsperlong.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/asm-generic/bitsperlong.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/asm-generic/bitsperlong.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/posix_types.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/stddef.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/stddef.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/posix_types.h \
    $(wildcard include/config/x86/32.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/uapi/asm/posix_types_64.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/asm-generic/posix_types.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/uuid.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/uuid.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/string.h \
    $(wildcard include/config/binary/printf.h) \
    $(wildcard include/config/fortify/source.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/compiler.h \
    $(wildcard include/config/trace/branch/profiling.h) \
    $(wildcard include/config/profile/all/branches.h) \
    $(wildcard include/config/stack/validation.h) \
    $(wildcard include/config/kasan.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/barrier.h \
    $(wildcard include/config/x86/ppro/fence.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/alternative.h \
    $(wildcard include/config/smp.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/stringify.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/asm.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/nops.h \
    $(wildcard include/config/mk7.h) \
    $(wildcard include/config/x86/p6/nop.h) \
    $(wildcard include/config/x86/64.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/asm-generic/barrier.h \
  /usr/lib64/clang/9.0.0/include/stdarg.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/string.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/string.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/string_64.h \
    $(wildcard include/config/x86/mce.h) \
    $(wildcard include/config/arch/has/uaccess/flushcache.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/jump_label.h \
    $(wildcard include/config/jump/label.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/atomic.h \
    $(wildcard include/config/generic/atomic64.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/atomic.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/cmpxchg.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/cpufeatures.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/required-features.h \
    $(wildcard include/config/x86/minimum/cpu/family.h) \
    $(wildcard include/config/math/emulation.h) \
    $(wildcard include/config/x86/pae.h) \
    $(wildcard include/config/x86/cmpxchg64.h) \
    $(wildcard include/config/x86/cmov.h) \
    $(wildcard include/config/x86/use/3dnow.h) \
    $(wildcard include/config/matom.h) \
    $(wildcard include/config/x86/5level.h) \
    $(wildcard include/config/paravirt.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/disabled-features.h \
    $(wildcard include/config/x86/intel/mpx.h) \
    $(wildcard include/config/x86/intel/memory/protection/keys.h) \
    $(wildcard include/config/page/table/isolation.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/cmpxchg_64.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/rmwcc.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/atomic64_64.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/asm-generic/atomic-long.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/bug.h \
    $(wildcard include/config/generic/bug.h) \
    $(wildcard include/config/bug/on/data/corruption.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/bug.h \
    $(wildcard include/config/debug/bugverbose.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/asm-generic/bug.h \
    $(wildcard include/config/bug.h) \
    $(wildcard include/config/generic/bug/relative/pointers.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/kernel.h \
    $(wildcard include/config/preempt/voluntary.h) \
    $(wildcard include/config/debug/atomic/sleep.h) \
    $(wildcard include/config/mmu.h) \
    $(wildcard include/config/prove/locking.h) \
    $(wildcard include/config/arch/has/refcount.h) \
    $(wildcard include/config/panic/timeout.h) \
    $(wildcard include/config/tracing.h) \
    $(wildcard include/config/ftrace/mcount/record.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/linkage.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/export.h \
    $(wildcard include/config/have/underscore/symbol/prefix.h) \
    $(wildcard include/config/modules.h) \
    $(wildcard include/config/modversions.h) \
    $(wildcard include/config/module/rel/crcs.h) \
    $(wildcard include/config/trim/unused/ksyms.h) \
    $(wildcard include/config/unused/symbols.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/linkage.h \
    $(wildcard include/config/x86/alignment/16.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/bitops.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/bits.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/bitops.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/asm-generic/bitops/find.h \
    $(wildcard include/config/generic/find/first/bit.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/asm-generic/bitops/sched.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/arch_hweight.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/asm-generic/bitops/const_hweight.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/asm-generic/bitops/le.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/uapi/asm/byteorder.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/byteorder/little_endian.h \
    $(wildcard include/config/cpu/big/endian.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/byteorder/little_endian.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/swab.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/swab.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/uapi/asm/swab.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/byteorder/generic.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/asm-generic/bitops/ext2-atomic-setbit.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/log2.h \
    $(wildcard include/config/arch/has/ilog2/u32.h) \
    $(wildcard include/config/arch/has/ilog2/u64.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/typecheck.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/printk.h \
    $(wildcard include/config/message/loglevel/default.h) \
    $(wildcard include/config/console/loglevel/default.h) \
    $(wildcard include/config/early/printk.h) \
    $(wildcard include/config/printk/nmi.h) \
    $(wildcard include/config/printk.h) \
    $(wildcard include/config/dynamic/debug.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/init.h \
    $(wildcard include/config/strict/kernel/rwx.h) \
    $(wildcard include/config/strict/module/rwx.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/kern_levels.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/cache.h \
    $(wildcard include/config/arch/has/cache/line/size.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/kernel.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/sysinfo.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/cache.h \
    $(wildcard include/config/x86/l1/cache/shift.h) \
    $(wildcard include/config/x86/internode/cache/shift.h) \
    $(wildcard include/config/x86/vsmp.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/build_bug.h \

scripts/mod/devicetable-offsets.s: $(deps_scripts/mod/devicetable-offsets.s)

$(deps_scripts/mod/devicetable-offsets.s):
