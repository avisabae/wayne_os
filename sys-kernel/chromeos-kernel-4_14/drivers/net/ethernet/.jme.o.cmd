cmd_drivers/net/ethernet/jme.o := x86_64-pc-linux-gnu-clang -B/usr/x86_64-pc-linux-gnu/binutils-bin/2.27.0 -Wp,-MD,drivers/net/ethernet/.jme.o.d  -nostdinc -isystem /usr/lib64/clang/9.0.0/include -I/var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include -I./arch/x86/include/generated  -I/var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include -I./include -I/var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/uapi -I./arch/x86/include/generated/uapi -I/var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi -I./include/generated/uapi -include /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/kconfig.h  -I/var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/drivers/net/ethernet -Idrivers/net/ethernet -D__KERNEL__ -Qunused-arguments -Wall -Wundef -Wstrict-prototypes -Wno-trigraphs -fno-strict-aliasing -fno-common -fshort-wchar -Werror-implicit-function-declaration -Wno-format-security -std=gnu89 --target=x86_64-pc-linux-gnu --prefix=/usr/bin/ --gcc-toolchain=/usr -no-integrated-as -fno-PIE -mno-sse -mno-mmx -mno-sse2 -mno-3dnow -mno-avx -m64 -mno-80387 -mstack-alignment=8 -mtune=generic -mno-red-zone -mcmodel=kernel -funit-at-a-time -DCONFIG_AS_CFI=1 -DCONFIG_AS_CFI_SIGNAL_FRAME=1 -DCONFIG_AS_CFI_SECTIONS=1 -DCONFIG_AS_FXSAVEQ=1 -DCONFIG_AS_SSSE3=1 -DCONFIG_AS_CRC32=1 -DCONFIG_AS_AVX=1 -DCONFIG_AS_AVX2=1 -DCONFIG_AS_AVX512=1 -DCONFIG_AS_SHA1_NI=1 -DCONFIG_AS_SHA256_NI=1 -pipe -Wno-sign-compare -fno-asynchronous-unwind-tables -mretpoline-external-thunk -fno-jump-tables -fno-delete-null-pointer-checks -Wno-frame-address -Wno-format-truncation -Wno-format-overflow -Wno-int-in-bool-context -Wno-attribute-alias -Os -Wno-maybe-uninitialized -Wno-maybe-uninitialized --param=allow-store-data-races=0 -Wframe-larger-than=2048 -fstack-protector-strong -Wno-format-invalid-specifier -Wno-gnu -Wno-address-of-packed-member -Wno-duplicate-decl-specifier -Wno-tautological-compare -Wno-constant-conversion -mno-global-merge -Wno-unused-const-variable -g -pg -mfentry -DCC_USING_FENTRY -Werror -Wdeclaration-after-statement -Wno-pointer-sign -Wno-stringop-truncation -fno-strict-overflow -fno-merge-all-constants -fno-stack-check -Werror=implicit-int -Werror=strict-prototypes -Werror=date-time -Werror=incompatible-pointer-types -Werror=designated-init -Wno-packed-not-aligned -Wno-initializer-overrides -Wno-unused-value -Wno-format -Wno-sign-compare -Wno-format-zero-length -Wno-uninitialized -fdebug-info-for-profiling  -DMODULE  -DKBUILD_BASENAME='"jme"'  -DKBUILD_MODNAME='"jme"' -c -o drivers/net/ethernet/jme.o /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/drivers/net/ethernet/jme.c

source_drivers/net/ethernet/jme.o := /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/drivers/net/ethernet/jme.c

deps_drivers/net/ethernet/jme.o := \
    $(wildcard include/config/net/poll/controller.h) \
    $(wildcard include/config/pm/sleep.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/compiler_types.h \
    $(wildcard include/config/have/arch/compiler/h.h) \
    $(wildcard include/config/enable/must/check.h) \
    $(wildcard include/config/enable/warn/deprecated.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/compiler-gcc.h \
    $(wildcard include/config/arch/supports/optimized/inlining.h) \
    $(wildcard include/config/optimize/inlining.h) \
    $(wildcard include/config/retpoline.h) \
    $(wildcard include/config/gcov/kernel.h) \
    $(wildcard include/config/arch/use/builtin/bswap.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/compiler-clang.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/module.h \
    $(wildcard include/config/modules.h) \
    $(wildcard include/config/sysfs.h) \
    $(wildcard include/config/modules/tree/lookup.h) \
    $(wildcard include/config/livepatch.h) \
    $(wildcard include/config/unused/symbols.h) \
    $(wildcard include/config/module/sig.h) \
    $(wildcard include/config/generic/bug.h) \
    $(wildcard include/config/kallsyms.h) \
    $(wildcard include/config/smp.h) \
    $(wildcard include/config/tracepoints.h) \
    $(wildcard include/config/tracing.h) \
    $(wildcard include/config/event/tracing.h) \
    $(wildcard include/config/ftrace/mcount/record.h) \
    $(wildcard include/config/module/unload.h) \
    $(wildcard include/config/constructors.h) \
    $(wildcard include/config/strict/module/rwx.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/list.h \
    $(wildcard include/config/debug/list.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/types.h \
    $(wildcard include/config/have/uid16.h) \
    $(wildcard include/config/uid16.h) \
    $(wildcard include/config/lbdaf.h) \
    $(wildcard include/config/arch/dma/addr/t/64bit.h) \
    $(wildcard include/config/phys/addr/t/64bit.h) \
    $(wildcard include/config/64bit.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/types.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/uapi/asm/types.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/asm-generic/types.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/asm-generic/int-ll64.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/asm-generic/int-ll64.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/uapi/asm/bitsperlong.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/asm-generic/bitsperlong.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/asm-generic/bitsperlong.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/posix_types.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/stddef.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/stddef.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/posix_types.h \
    $(wildcard include/config/x86/32.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/uapi/asm/posix_types_64.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/asm-generic/posix_types.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/poison.h \
    $(wildcard include/config/illegal/pointer/value.h) \
    $(wildcard include/config/page/poisoning/zero.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/const.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/kernel.h \
    $(wildcard include/config/preempt/voluntary.h) \
    $(wildcard include/config/debug/atomic/sleep.h) \
    $(wildcard include/config/mmu.h) \
    $(wildcard include/config/prove/locking.h) \
    $(wildcard include/config/arch/has/refcount.h) \
    $(wildcard include/config/panic/timeout.h) \
  /usr/lib64/clang/9.0.0/include/stdarg.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/linkage.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/stringify.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/export.h \
    $(wildcard include/config/have/underscore/symbol/prefix.h) \
    $(wildcard include/config/modversions.h) \
    $(wildcard include/config/module/rel/crcs.h) \
    $(wildcard include/config/trim/unused/ksyms.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/linkage.h \
    $(wildcard include/config/x86/64.h) \
    $(wildcard include/config/x86/alignment/16.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/compiler.h \
    $(wildcard include/config/trace/branch/profiling.h) \
    $(wildcard include/config/profile/all/branches.h) \
    $(wildcard include/config/stack/validation.h) \
    $(wildcard include/config/kasan.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/barrier.h \
    $(wildcard include/config/x86/ppro/fence.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/alternative.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/asm.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/nops.h \
    $(wildcard include/config/mk7.h) \
    $(wildcard include/config/x86/p6/nop.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/asm-generic/barrier.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/bitops.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/bits.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/bitops.h \
    $(wildcard include/config/x86/cmov.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/rmwcc.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/asm-generic/bitops/find.h \
    $(wildcard include/config/generic/find/first/bit.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/asm-generic/bitops/sched.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/arch_hweight.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/cpufeatures.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/required-features.h \
    $(wildcard include/config/x86/minimum/cpu/family.h) \
    $(wildcard include/config/math/emulation.h) \
    $(wildcard include/config/x86/pae.h) \
    $(wildcard include/config/x86/cmpxchg64.h) \
    $(wildcard include/config/x86/use/3dnow.h) \
    $(wildcard include/config/matom.h) \
    $(wildcard include/config/x86/5level.h) \
    $(wildcard include/config/paravirt.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/disabled-features.h \
    $(wildcard include/config/x86/intel/mpx.h) \
    $(wildcard include/config/x86/intel/memory/protection/keys.h) \
    $(wildcard include/config/page/table/isolation.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/asm-generic/bitops/const_hweight.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/asm-generic/bitops/le.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/uapi/asm/byteorder.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/byteorder/little_endian.h \
    $(wildcard include/config/cpu/big/endian.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/byteorder/little_endian.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/swab.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/swab.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/uapi/asm/swab.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/byteorder/generic.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/asm-generic/bitops/ext2-atomic-setbit.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/log2.h \
    $(wildcard include/config/arch/has/ilog2/u32.h) \
    $(wildcard include/config/arch/has/ilog2/u64.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/typecheck.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/printk.h \
    $(wildcard include/config/message/loglevel/default.h) \
    $(wildcard include/config/console/loglevel/default.h) \
    $(wildcard include/config/early/printk.h) \
    $(wildcard include/config/printk/nmi.h) \
    $(wildcard include/config/printk.h) \
    $(wildcard include/config/dynamic/debug.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/init.h \
    $(wildcard include/config/strict/kernel/rwx.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/kern_levels.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/cache.h \
    $(wildcard include/config/arch/has/cache/line/size.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/kernel.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/sysinfo.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/cache.h \
    $(wildcard include/config/x86/l1/cache/shift.h) \
    $(wildcard include/config/x86/internode/cache/shift.h) \
    $(wildcard include/config/x86/vsmp.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/build_bug.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/stat.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/uapi/asm/stat.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/stat.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/time.h \
    $(wildcard include/config/arch/uses/gettimeoffset.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/seqlock.h \
    $(wildcard include/config/debug/lock/alloc.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/spinlock.h \
    $(wildcard include/config/debug/spinlock.h) \
    $(wildcard include/config/generic/lockbreak.h) \
    $(wildcard include/config/preempt.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/preempt.h \
    $(wildcard include/config/preempt/count.h) \
    $(wildcard include/config/debug/preempt.h) \
    $(wildcard include/config/preempt/tracer.h) \
    $(wildcard include/config/preempt/notifiers.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/preempt.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/percpu.h \
    $(wildcard include/config/x86/64/smp.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/asm-generic/percpu.h \
    $(wildcard include/config/have/setup/per/cpu/area.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/threads.h \
    $(wildcard include/config/nr/cpus.h) \
    $(wildcard include/config/base/small.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/percpu-defs.h \
    $(wildcard include/config/debug/force/weak/per/cpu.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/thread_info.h \
    $(wildcard include/config/thread/info/in/task.h) \
    $(wildcard include/config/have/arch/within/stack/frames.h) \
    $(wildcard include/config/hardened/usercopy.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/bug.h \
    $(wildcard include/config/bug/on/data/corruption.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/bug.h \
    $(wildcard include/config/debug/bugverbose.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/asm-generic/bug.h \
    $(wildcard include/config/bug.h) \
    $(wildcard include/config/generic/bug/relative/pointers.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/restart_block.h \
    $(wildcard include/config/compat.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/current.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/thread_info.h \
    $(wildcard include/config/vm86.h) \
    $(wildcard include/config/alt/syscall.h) \
    $(wildcard include/config/ia32/emulation.h) \
    $(wildcard include/config/frame/pointer.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/page.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/page_types.h \
    $(wildcard include/config/physical/start.h) \
    $(wildcard include/config/physical/align.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/mem_encrypt.h \
    $(wildcard include/config/arch/has/mem/encrypt.h) \
    $(wildcard include/config/amd/mem/encrypt.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/mem_encrypt.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/uapi/asm/bootparam.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/screen_info.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/screen_info.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/apm_bios.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/apm_bios.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/ioctl.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/uapi/asm/ioctl.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/asm-generic/ioctl.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/asm-generic/ioctl.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/edd.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/edd.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/ist.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/uapi/asm/ist.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/video/edid.h \
    $(wildcard include/config/x86.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/video/edid.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/page_64_types.h \
    $(wildcard include/config/kasan/extra.h) \
    $(wildcard include/config/randomize/memory.h) \
    $(wildcard include/config/randomize/base.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/kaslr.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/page_64.h \
    $(wildcard include/config/debug/virtual.h) \
    $(wildcard include/config/flatmem.h) \
    $(wildcard include/config/x86/vsyscall/emulation.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/range.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/asm-generic/memory_model.h \
    $(wildcard include/config/discontigmem.h) \
    $(wildcard include/config/sparsemem/vmemmap.h) \
    $(wildcard include/config/sparsemem.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/pfn.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/asm-generic/getorder.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/cpufeature.h \
    $(wildcard include/config/x86/feature/names.h) \
    $(wildcard include/config/x86/fast/feature/tests.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/processor.h \
    $(wildcard include/config/cc/stackprotector.h) \
    $(wildcard include/config/x86/debugctlmsr.h) \
    $(wildcard include/config/cpu/sup/amd.h) \
    $(wildcard include/config/xen.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/processor-flags.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/uapi/asm/processor-flags.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/math_emu.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/ptrace.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/segment.h \
    $(wildcard include/config/xen/pv.h) \
    $(wildcard include/config/x86/32/lazy/gs.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/uapi/asm/ptrace.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/uapi/asm/ptrace-abi.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/asm-generic/ptrace.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/uapi/asm/sigcontext.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/pgtable_types.h \
    $(wildcard include/config/mem/soft/dirty.h) \
    $(wildcard include/config/pgtable/levels.h) \
    $(wildcard include/config/proc/fs.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/pgtable_64_types.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/sparsemem.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/asm-generic/pgtable-nop4d.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/msr.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/msr-index.h \
    $(wildcard include/config/control.h) \
    $(wildcard include/config/tdp/nominal.h) \
    $(wildcard include/config/tdp/level/1.h) \
    $(wildcard include/config/tdp/level/2.h) \
    $(wildcard include/config/tdp/control.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/uapi/asm/errno.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/asm-generic/errno.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/asm-generic/errno-base.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/cpumask.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/cpumask.h \
    $(wildcard include/config/cpumask/offstack.h) \
    $(wildcard include/config/hotplug/cpu.h) \
    $(wildcard include/config/debug/per/cpu/maps.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/bitmap.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/string.h \
    $(wildcard include/config/binary/printf.h) \
    $(wildcard include/config/fortify/source.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/string.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/string.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/string_64.h \
    $(wildcard include/config/x86/mce.h) \
    $(wildcard include/config/arch/has/uaccess/flushcache.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/jump_label.h \
    $(wildcard include/config/jump/label.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/atomic.h \
    $(wildcard include/config/generic/atomic64.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/atomic.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/cmpxchg.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/cmpxchg_64.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/atomic64_64.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/asm-generic/atomic-long.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/uapi/asm/msr.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/tracepoint-defs.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/static_key.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/errno.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/errno.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/desc_defs.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/special_insns.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/fpu/types.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/unwind_hints.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/orc_types.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/personality.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/personality.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/math64.h \
    $(wildcard include/config/arch/supports/int128.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/div64.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/asm-generic/div64.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/err.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/irqflags.h \
    $(wildcard include/config/trace/irqflags.h) \
    $(wildcard include/config/irqsoff/tracer.h) \
    $(wildcard include/config/trace/irqflags/support.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/irqflags.h \
    $(wildcard include/config/debug/entry.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/nospec-branch.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/alternative-asm.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/bottom_half.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/spinlock_types.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/spinlock_types.h \
    $(wildcard include/config/paravirt/spinlocks.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/asm-generic/qspinlock_types.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/asm-generic/qrwlock_types.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/lockdep.h \
    $(wildcard include/config/lockdep.h) \
    $(wildcard include/config/lock/stat.h) \
    $(wildcard include/config/lockdep/crossrelease.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/rwlock_types.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/spinlock.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/paravirt.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/qspinlock.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/asm-generic/qspinlock.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/qrwlock.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/asm-generic/qrwlock.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/rwlock.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/spinlock_api_smp.h \
    $(wildcard include/config/inline/spin/lock.h) \
    $(wildcard include/config/inline/spin/lock/bh.h) \
    $(wildcard include/config/inline/spin/lock/irq.h) \
    $(wildcard include/config/inline/spin/lock/irqsave.h) \
    $(wildcard include/config/inline/spin/trylock.h) \
    $(wildcard include/config/inline/spin/trylock/bh.h) \
    $(wildcard include/config/uninline/spin/unlock.h) \
    $(wildcard include/config/inline/spin/unlock/bh.h) \
    $(wildcard include/config/inline/spin/unlock/irq.h) \
    $(wildcard include/config/inline/spin/unlock/irqrestore.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/rwlock_api_smp.h \
    $(wildcard include/config/inline/read/lock.h) \
    $(wildcard include/config/inline/write/lock.h) \
    $(wildcard include/config/inline/read/lock/bh.h) \
    $(wildcard include/config/inline/write/lock/bh.h) \
    $(wildcard include/config/inline/read/lock/irq.h) \
    $(wildcard include/config/inline/write/lock/irq.h) \
    $(wildcard include/config/inline/read/lock/irqsave.h) \
    $(wildcard include/config/inline/write/lock/irqsave.h) \
    $(wildcard include/config/inline/read/trylock.h) \
    $(wildcard include/config/inline/write/trylock.h) \
    $(wildcard include/config/inline/read/unlock.h) \
    $(wildcard include/config/inline/write/unlock.h) \
    $(wildcard include/config/inline/read/unlock/bh.h) \
    $(wildcard include/config/inline/write/unlock/bh.h) \
    $(wildcard include/config/inline/read/unlock/irq.h) \
    $(wildcard include/config/inline/write/unlock/irq.h) \
    $(wildcard include/config/inline/read/unlock/irqrestore.h) \
    $(wildcard include/config/inline/write/unlock/irqrestore.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/time64.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/time.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/uidgid.h \
    $(wildcard include/config/multiuser.h) \
    $(wildcard include/config/user/ns.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/highuid.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/kmod.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/umh.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/gfp.h \
    $(wildcard include/config/highmem.h) \
    $(wildcard include/config/zone/dma.h) \
    $(wildcard include/config/zone/dma32.h) \
    $(wildcard include/config/zone/device.h) \
    $(wildcard include/config/numa.h) \
    $(wildcard include/config/memory/isolation.h) \
    $(wildcard include/config/compaction.h) \
    $(wildcard include/config/cma.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/mmdebug.h \
    $(wildcard include/config/debug/vm.h) \
    $(wildcard include/config/debug/vm/pgflags.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/mmzone.h \
    $(wildcard include/config/force/max/zoneorder.h) \
    $(wildcard include/config/zsmalloc.h) \
    $(wildcard include/config/memcg.h) \
    $(wildcard include/config/memory/hotplug.h) \
    $(wildcard include/config/flat/node/mem/map.h) \
    $(wildcard include/config/page/extension.h) \
    $(wildcard include/config/no/bootmem.h) \
    $(wildcard include/config/numa/balancing.h) \
    $(wildcard include/config/deferred/struct/page/init.h) \
    $(wildcard include/config/transparent/hugepage.h) \
    $(wildcard include/config/have/memory/present.h) \
    $(wildcard include/config/have/memoryless/nodes.h) \
    $(wildcard include/config/need/node/memmap/size.h) \
    $(wildcard include/config/have/memblock/node/map.h) \
    $(wildcard include/config/need/multiple/nodes.h) \
    $(wildcard include/config/have/arch/early/pfn/to/nid.h) \
    $(wildcard include/config/sparsemem/extreme.h) \
    $(wildcard include/config/memory/hotremove.h) \
    $(wildcard include/config/have/arch/pfn/valid.h) \
    $(wildcard include/config/holes/in/zone.h) \
    $(wildcard include/config/arch/has/holes/memorymodel.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/wait.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/wait.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/numa.h \
    $(wildcard include/config/nodes/shift.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/nodemask.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/pageblock-flags.h \
    $(wildcard include/config/hugetlb/page.h) \
    $(wildcard include/config/hugetlb/page/size/variable.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/page-flags-layout.h \
  include/generated/bounds.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/memory_hotplug.h \
    $(wildcard include/config/arch/has/add/pages.h) \
    $(wildcard include/config/have/arch/nodedata/extension.h) \
    $(wildcard include/config/have/bootmem/info/node.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/notifier.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/mutex.h \
    $(wildcard include/config/mutex/spin/on/owner.h) \
    $(wildcard include/config/debug/mutexes.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/osq_lock.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/debug_locks.h \
    $(wildcard include/config/debug/locking/api/selftests.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/rwsem.h \
    $(wildcard include/config/rwsem/spin/on/owner.h) \
    $(wildcard include/config/rwsem/generic/spinlock.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/rwsem.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/srcu.h \
    $(wildcard include/config/tiny/srcu.h) \
    $(wildcard include/config/tree/srcu.h) \
    $(wildcard include/config/srcu.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/rcupdate.h \
    $(wildcard include/config/preempt/rcu.h) \
    $(wildcard include/config/rcu/stall/common.h) \
    $(wildcard include/config/no/hz/full.h) \
    $(wildcard include/config/rcu/nocb/cpu.h) \
    $(wildcard include/config/tasks/rcu.h) \
    $(wildcard include/config/tree/rcu.h) \
    $(wildcard include/config/tiny/rcu.h) \
    $(wildcard include/config/debug/objects/rcu/head.h) \
    $(wildcard include/config/prove/rcu.h) \
    $(wildcard include/config/rcu/boost.h) \
    $(wildcard include/config/arch/weak/release/acquire.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/rcutree.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/workqueue.h \
    $(wildcard include/config/debug/objects/work.h) \
    $(wildcard include/config/freezer.h) \
    $(wildcard include/config/wq/watchdog.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/timer.h \
    $(wildcard include/config/debug/objects/timers.h) \
    $(wildcard include/config/no/hz/common.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/ktime.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/jiffies.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/timex.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/timex.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/param.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/uapi/asm/param.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/asm-generic/param.h \
    $(wildcard include/config/hz.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/asm-generic/param.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/timex.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/tsc.h \
    $(wildcard include/config/x86/tsc.h) \
  include/generated/timeconst.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/timekeeping.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/debugobjects.h \
    $(wildcard include/config/debug/objects.h) \
    $(wildcard include/config/debug/objects/free.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/rcu_segcblist.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/srcutree.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/rcu_node_tree.h \
    $(wildcard include/config/rcu/fanout.h) \
    $(wildcard include/config/rcu/fanout/leaf.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/completion.h \
    $(wildcard include/config/lockdep/completions.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/topology.h \
    $(wildcard include/config/use/percpu/numa/node/id.h) \
    $(wildcard include/config/sched/smt.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/smp.h \
    $(wildcard include/config/up/late/init.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/llist.h \
    $(wildcard include/config/arch/have/nmi/safe/cmpxchg.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/smp.h \
    $(wildcard include/config/x86/local/apic.h) \
    $(wildcard include/config/x86/io/apic.h) \
    $(wildcard include/config/debug/nmi/selftest.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/mpspec.h \
    $(wildcard include/config/eisa.h) \
    $(wildcard include/config/x86/mpparse.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/mpspec_def.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/x86_init.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/apicdef.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/apic.h \
    $(wildcard include/config/x86/x2apic.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/fixmap.h \
    $(wildcard include/config/provide/ohci1394/dma/init.h) \
    $(wildcard include/config/pci/mmconfig.h) \
    $(wildcard include/config/x86/intel/mid.h) \
    $(wildcard include/config/acpi/apei/ghes.h) \
    $(wildcard include/config/intel/txt.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/acpi.h \
    $(wildcard include/config/acpi/apei.h) \
    $(wildcard include/config/acpi.h) \
    $(wildcard include/config/acpi/numa.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/acpi/pdc_intel.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/numa.h \
    $(wildcard include/config/numa/emu.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/topology.h \
    $(wildcard include/config/sched/mc/prio.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/asm-generic/topology.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/mmu.h \
    $(wildcard include/config/modify/ldt/syscall.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/realmode.h \
    $(wildcard include/config/acpi/sleep.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/io.h \
    $(wildcard include/config/mtrr.h) \
    $(wildcard include/config/x86/pat.h) \
  arch/x86/include/generated/asm/early_ioremap.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/asm-generic/early_ioremap.h \
    $(wildcard include/config/generic/early/ioremap.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/asm-generic/iomap.h \
    $(wildcard include/config/has/ioport/map.h) \
    $(wildcard include/config/pci.h) \
    $(wildcard include/config/generic/iomap.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/asm-generic/pci_iomap.h \
    $(wildcard include/config/no/generic/pci/ioport/map.h) \
    $(wildcard include/config/generic/pci/iomap.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/asm-generic/io.h \
    $(wildcard include/config/virt/to/bus.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/vmalloc.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/rbtree.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/uapi/asm/vsyscall.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/asm-generic/fixmap.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/hardirq.h \
    $(wildcard include/config/kvm/intel.h) \
    $(wildcard include/config/have/kvm.h) \
    $(wildcard include/config/x86/thermal/vector.h) \
    $(wildcard include/config/x86/mce/threshold.h) \
    $(wildcard include/config/x86/mce/amd.h) \
    $(wildcard include/config/hyperv.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/io_apic.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/irq_vectors.h \
    $(wildcard include/config/pci/msi.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/percpu.h \
    $(wildcard include/config/need/per/cpu/embed/first/chunk.h) \
    $(wildcard include/config/need/per/cpu/page/first/chunk.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/sysctl.h \
    $(wildcard include/config/sysctl.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/sysctl.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/elf.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/elf.h \
    $(wildcard include/config/x86/x32/abi.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/user.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/user_64.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/uapi/asm/auxvec.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/vdso.h \
    $(wildcard include/config/x86/x32.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/mm_types.h \
    $(wildcard include/config/have/cmpxchg/double.h) \
    $(wildcard include/config/have/aligned/struct/page.h) \
    $(wildcard include/config/userfaultfd.h) \
    $(wildcard include/config/have/arch/compat/mmap/bases.h) \
    $(wildcard include/config/membarrier.h) \
    $(wildcard include/config/aio.h) \
    $(wildcard include/config/mmu/notifier.h) \
    $(wildcard include/config/arch/want/batched/unmap/tlb/flush.h) \
    $(wildcard include/config/hmm.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/mm_types_task.h \
    $(wildcard include/config/split/ptlock/cpus.h) \
    $(wildcard include/config/arch/enable/split/pmd/ptlock.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/tlbbatch.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/auxvec.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/auxvec.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/uprobes.h \
    $(wildcard include/config/uprobes.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/uprobes.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/elf.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/elf-em.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/kobject.h \
    $(wildcard include/config/uevent/helper.h) \
    $(wildcard include/config/debug/kobject/release.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/sysfs.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/kernfs.h \
    $(wildcard include/config/kernfs.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/idr.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/radix-tree.h \
    $(wildcard include/config/radix/tree/multiorder.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/kobject_ns.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/kref.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/refcount.h \
    $(wildcard include/config/refcount/full.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/refcount.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/moduleparam.h \
    $(wildcard include/config/alpha.h) \
    $(wildcard include/config/ia64.h) \
    $(wildcard include/config/ppc64.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/rbtree_latch.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/module.h \
    $(wildcard include/config/unwinder/orc.h) \
    $(wildcard include/config/m486.h) \
    $(wildcard include/config/m586.h) \
    $(wildcard include/config/m586tsc.h) \
    $(wildcard include/config/m586mmx.h) \
    $(wildcard include/config/mcore2.h) \
    $(wildcard include/config/m686.h) \
    $(wildcard include/config/mpentiumii.h) \
    $(wildcard include/config/mpentiumiii.h) \
    $(wildcard include/config/mpentiumm.h) \
    $(wildcard include/config/mpentium4.h) \
    $(wildcard include/config/mk6.h) \
    $(wildcard include/config/mk8.h) \
    $(wildcard include/config/melan.h) \
    $(wildcard include/config/mcrusoe.h) \
    $(wildcard include/config/mefficeon.h) \
    $(wildcard include/config/mwinchipc6.h) \
    $(wildcard include/config/mwinchip3d.h) \
    $(wildcard include/config/mcyrixiii.h) \
    $(wildcard include/config/mviac3/2.h) \
    $(wildcard include/config/mviac7.h) \
    $(wildcard include/config/mgeodegx1.h) \
    $(wildcard include/config/mgeode/lx.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/asm-generic/module.h \
    $(wildcard include/config/have/mod/arch/specific.h) \
    $(wildcard include/config/modules/use/elf/rel.h) \
    $(wildcard include/config/modules/use/elf/rela.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/pci.h \
    $(wildcard include/config/pci/iov.h) \
    $(wildcard include/config/pcieaer.h) \
    $(wildcard include/config/pcieaspm.h) \
    $(wildcard include/config/pcie/ptm.h) \
    $(wildcard include/config/pci/ats.h) \
    $(wildcard include/config/pci/pri.h) \
    $(wildcard include/config/pci/pasid.h) \
    $(wildcard include/config/pci/domains/generic.h) \
    $(wildcard include/config/pci/bus/addr/t/64bit.h) \
    $(wildcard include/config/pcieportbus.h) \
    $(wildcard include/config/pcie/ecrc.h) \
    $(wildcard include/config/ht/irq.h) \
    $(wildcard include/config/pci/domains.h) \
    $(wildcard include/config/pci/quirks.h) \
    $(wildcard include/config/hibernate/callbacks.h) \
    $(wildcard include/config/acpi/mcfg.h) \
    $(wildcard include/config/hotplug/pci.h) \
    $(wildcard include/config/of.h) \
    $(wildcard include/config/eeh.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/mod_devicetable.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/uuid.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/uuid.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/ioport.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/device.h \
    $(wildcard include/config/debug/devres.h) \
    $(wildcard include/config/generic/msi/irq/domain.h) \
    $(wildcard include/config/pinctrl.h) \
    $(wildcard include/config/generic/msi/irq.h) \
    $(wildcard include/config/dma/cma.h) \
    $(wildcard include/config/devtmpfs.h) \
    $(wildcard include/config/sysfs/deprecated.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/klist.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/pinctrl/devinfo.h \
    $(wildcard include/config/pm.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/pinctrl/consumer.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/seq_file.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/fs.h \
    $(wildcard include/config/fs/posix/acl.h) \
    $(wildcard include/config/security.h) \
    $(wildcard include/config/cgroup/writeback.h) \
    $(wildcard include/config/ima.h) \
    $(wildcard include/config/fsnotify.h) \
    $(wildcard include/config/fs/encryption.h) \
    $(wildcard include/config/epoll.h) \
    $(wildcard include/config/file/locking.h) \
    $(wildcard include/config/quota.h) \
    $(wildcard include/config/fs/dax.h) \
    $(wildcard include/config/mandatory/file/locking.h) \
    $(wildcard include/config/block.h) \
    $(wildcard include/config/migration.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/wait_bit.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/kdev_t.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/kdev_t.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/dcache.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/rculist.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/rculist_bl.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/list_bl.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/bit_spinlock.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/lockref.h \
    $(wildcard include/config/arch/use/cmpxchg/lockref.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/stringhash.h \
    $(wildcard include/config/dcache/word/access.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/hash.h \
    $(wildcard include/config/have/arch/hash.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/path.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/list_lru.h \
    $(wildcard include/config/slob.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/shrinker.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/pid.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/capability.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/capability.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/semaphore.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/fcntl.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/fcntl.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/uapi/asm/fcntl.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/asm-generic/fcntl.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/fiemap.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/migrate_mode.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/percpu-rwsem.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/rcuwait.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/rcu_sync.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/delayed_call.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/errseq.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/fs.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/limits.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/quota.h \
    $(wildcard include/config/quota/netlink/interface.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/percpu_counter.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/dqblk_xfs.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/dqblk_v1.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/dqblk_v2.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/dqblk_qtree.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/projid.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/quota.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/nfs_fs_i.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/cred.h \
    $(wildcard include/config/debug/credentials.h) \
    $(wildcard include/config/keys.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/key.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/assoc_array.h \
    $(wildcard include/config/associative/array.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/selinux.h \
    $(wildcard include/config/security/selinux.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/sched.h \
    $(wildcard include/config/virt/cpu/accounting/native.h) \
    $(wildcard include/config/sched/info.h) \
    $(wildcard include/config/schedstats.h) \
    $(wildcard include/config/fair/group/sched.h) \
    $(wildcard include/config/sched/walt.h) \
    $(wildcard include/config/rt/group/sched.h) \
    $(wildcard include/config/cgroup/sched.h) \
    $(wildcard include/config/blk/dev/io/trace.h) \
    $(wildcard include/config/compat/brk.h) \
    $(wildcard include/config/cgroups.h) \
    $(wildcard include/config/arch/has/scaled/cputime.h) \
    $(wildcard include/config/cpu/freq/times.h) \
    $(wildcard include/config/virt/cpu/accounting/gen.h) \
    $(wildcard include/config/posix/timers.h) \
    $(wildcard include/config/sysvipc.h) \
    $(wildcard include/config/detect/hung/task.h) \
    $(wildcard include/config/auditsyscall.h) \
    $(wildcard include/config/rt/mutexes.h) \
    $(wildcard include/config/ubsan.h) \
    $(wildcard include/config/task/xacct.h) \
    $(wildcard include/config/cpusets.h) \
    $(wildcard include/config/intel/rdt.h) \
    $(wildcard include/config/futex.h) \
    $(wildcard include/config/perf/events.h) \
    $(wildcard include/config/task/delay/acct.h) \
    $(wildcard include/config/fault/injection.h) \
    $(wildcard include/config/latencytop.h) \
    $(wildcard include/config/function/graph/tracer.h) \
    $(wildcard include/config/kcov.h) \
    $(wildcard include/config/bcache.h) \
    $(wildcard include/config/vmap/stack.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/sched.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/sem.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/sem.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/ipc.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/rhashtable.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/jhash.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/unaligned/packed_struct.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/list_nulls.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/ipc.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/uapi/asm/ipcbuf.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/asm-generic/ipcbuf.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/uapi/asm/sembuf.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/shm.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/shm.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/asm-generic/hugetlb_encode.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/uapi/asm/shmbuf.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/asm-generic/shmbuf.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/shmparam.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/kcov.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/kcov.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/plist.h \
    $(wildcard include/config/debug/pi/list.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/hrtimer.h \
    $(wildcard include/config/high/res/timers.h) \
    $(wildcard include/config/time/low/res.h) \
    $(wildcard include/config/timerfd.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/timerqueue.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/seccomp.h \
    $(wildcard include/config/seccomp.h) \
    $(wildcard include/config/have/arch/seccomp/filter.h) \
    $(wildcard include/config/seccomp/filter.h) \
    $(wildcard include/config/checkpoint/restore.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/seccomp.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/seccomp.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/unistd.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/uapi/asm/unistd.h \
  arch/x86/include/generated/uapi/asm/unistd_64.h \
  arch/x86/include/generated/asm/unistd_64_x32.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/ia32_unistd.h \
  arch/x86/include/generated/asm/unistd_32_ia32.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/asm-generic/seccomp.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/unistd.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/resource.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/resource.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/uapi/asm/resource.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/asm-generic/resource.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/asm-generic/resource.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/latencytop.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/sched/prio.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/signal_types.h \
    $(wildcard include/config/old/sigaction.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/signal.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/signal.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/uapi/asm/signal.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/asm-generic/signal-defs.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/uapi/asm/siginfo.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/asm-generic/siginfo.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/task_io_accounting.h \
    $(wildcard include/config/task/io/accounting.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/sched/user.h \
    $(wildcard include/config/fanotify.h) \
    $(wildcard include/config/posix/mqueue.h) \
    $(wildcard include/config/bpf/syscall.h) \
    $(wildcard include/config/net.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/pinctrl/pinctrl-state.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/pm.h \
    $(wildcard include/config/vt/console/sleep.h) \
    $(wildcard include/config/pm/clk.h) \
    $(wildcard include/config/pm/generic/domains.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/ratelimit.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/overflow.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/device.h \
    $(wildcard include/config/intel/iommu.h) \
    $(wildcard include/config/amd/iommu.h) \
    $(wildcard include/config/x86/dev/dma/ops.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/pm_wakeup.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/interrupt.h \
    $(wildcard include/config/irq/forced/threading.h) \
    $(wildcard include/config/generic/irq/probe.h) \
    $(wildcard include/config/irq/timings.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/irqreturn.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/irqnr.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/irqnr.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/hardirq.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/ftrace_irq.h \
    $(wildcard include/config/ftrace/nmi/enter.h) \
    $(wildcard include/config/hwlat/tracer.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/vtime.h \
    $(wildcard include/config/virt/cpu/accounting.h) \
    $(wildcard include/config/irq/time/accounting.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/context_tracking_state.h \
    $(wildcard include/config/context/tracking.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/irq.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/sections.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/asm-generic/sections.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/extable.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/io.h \
    $(wildcard include/config/have/arch/huge/vmap.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/resource_ext.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/slab.h \
    $(wildcard include/config/debug/slab.h) \
    $(wildcard include/config/failslab.h) \
    $(wildcard include/config/have/hardened/usercopy/allocator.h) \
    $(wildcard include/config/slab.h) \
    $(wildcard include/config/slub.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/kmemleak.h \
    $(wildcard include/config/debug/kmemleak.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/kasan.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/pci.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/pci_regs.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/pci_ids.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/pci-dma.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/dmapool.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/scatterlist.h \
    $(wildcard include/config/debug/sg.h) \
    $(wildcard include/config/need/sg/dma/length.h) \
    $(wildcard include/config/arch/has/sg/chain.h) \
    $(wildcard include/config/sg/pool.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/mm.h \
    $(wildcard include/config/have/arch/mmap/rnd/bits.h) \
    $(wildcard include/config/have/arch/mmap/rnd/compat/bits.h) \
    $(wildcard include/config/arch/uses/high/vma/flags.h) \
    $(wildcard include/config/ppc.h) \
    $(wildcard include/config/parisc.h) \
    $(wildcard include/config/metag.h) \
    $(wildcard include/config/stack/growsup.h) \
    $(wildcard include/config/device/private.h) \
    $(wildcard include/config/device/public.h) \
    $(wildcard include/config/shmem.h) \
    $(wildcard include/config/debug/vm/rb.h) \
    $(wildcard include/config/page/poisoning.h) \
    $(wildcard include/config/debug/pagealloc.h) \
    $(wildcard include/config/hibernation.h) \
    $(wildcard include/config/hugetlbfs.h) \
    $(wildcard include/config/disk/based/swap.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/percpu-refcount.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/page_ext.h \
    $(wildcard include/config/idle/page/tracking.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/stacktrace.h \
    $(wildcard include/config/stacktrace.h) \
    $(wildcard include/config/user/stacktrace/support.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/stackdepot.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/page_ref.h \
    $(wildcard include/config/debug/page/ref.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/page-flags.h \
    $(wildcard include/config/arch/uses/pg/uncached.h) \
    $(wildcard include/config/memory/failure.h) \
    $(wildcard include/config/swap.h) \
    $(wildcard include/config/thp/swap.h) \
    $(wildcard include/config/ksm.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/memremap.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/pgtable.h \
    $(wildcard include/config/debug/wx.h) \
    $(wildcard include/config/have/arch/transparent/hugepage/pud.h) \
    $(wildcard include/config/have/arch/soft/dirty.h) \
    $(wildcard include/config/arch/enable/thp/migration.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/pgtable_64.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/pgtable-invert.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/asm-generic/pgtable.h \
    $(wildcard include/config/x86/espfix64.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/huge_mm.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/sched/coredump.h \
    $(wildcard include/config/core/dump/default/elf/headers.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/vmstat.h \
    $(wildcard include/config/vm/event/counters.h) \
    $(wildcard include/config/debug/tlbflush.h) \
    $(wildcard include/config/debug/vm/vmacache.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/vm_event_item.h \
    $(wildcard include/config/memory/balloon.h) \
    $(wildcard include/config/balloon/compaction.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/pci.h \
    $(wildcard include/config/pci/msi/irq/domain.h) \
    $(wildcard include/config/vmd.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/pat.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/pci_64.h \
    $(wildcard include/config/calgary/iommu.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/asm-generic/pci.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/pci-dma-compat.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/dma-mapping.h \
    $(wildcard include/config/have/generic/dma/coherent.h) \
    $(wildcard include/config/has/dma.h) \
    $(wildcard include/config/arch/has/dma/set/coherent/mask.h) \
    $(wildcard include/config/need/dma/map/state.h) \
    $(wildcard include/config/dma/api/debug.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/sizes.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/dma-debug.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/dma-direction.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/dma-mapping.h \
    $(wildcard include/config/isa.h) \
    $(wildcard include/config/x86/dma/remap.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/swiotlb.h \
    $(wildcard include/config/swiotlb.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/swiotlb.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/dma-contiguous.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/pci-aspm.h \
    $(wildcard include/config/pcieaspm/debug.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/netdevice.h \
    $(wildcard include/config/dcb.h) \
    $(wildcard include/config/hyperv/net.h) \
    $(wildcard include/config/wlan.h) \
    $(wildcard include/config/ax25.h) \
    $(wildcard include/config/mac80211/mesh.h) \
    $(wildcard include/config/net/ipip.h) \
    $(wildcard include/config/net/ipgre.h) \
    $(wildcard include/config/ipv6/sit.h) \
    $(wildcard include/config/ipv6/tunnel.h) \
    $(wildcard include/config/rps.h) \
    $(wildcard include/config/netpoll.h) \
    $(wildcard include/config/xps.h) \
    $(wildcard include/config/bql.h) \
    $(wildcard include/config/rfs/accel.h) \
    $(wildcard include/config/fcoe.h) \
    $(wildcard include/config/xfrm/offload.h) \
    $(wildcard include/config/libfcoe.h) \
    $(wildcard include/config/wireless/ext.h) \
    $(wildcard include/config/net/switchdev.h) \
    $(wildcard include/config/net/l3/master/dev.h) \
    $(wildcard include/config/ipv6.h) \
    $(wildcard include/config/xfrm.h) \
    $(wildcard include/config/vlan/8021q.h) \
    $(wildcard include/config/net/dsa.h) \
    $(wildcard include/config/tipc.h) \
    $(wildcard include/config/mpls/routing.h) \
    $(wildcard include/config/net/cls/act.h) \
    $(wildcard include/config/netfilter/ingress.h) \
    $(wildcard include/config/net/sched.h) \
    $(wildcard include/config/garp.h) \
    $(wildcard include/config/mrp.h) \
    $(wildcard include/config/cgroup/net/prio.h) \
    $(wildcard include/config/net/flow/limit.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/delay.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/delay.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/asm-generic/delay.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/prefetch.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/dynamic_queue_limits.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/ethtool.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/compat.h \
    $(wildcard include/config/compat/old/sigaction.h) \
    $(wildcard include/config/odd/rt/sigaction.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/socket.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/uapi/asm/socket.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/asm-generic/socket.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/uapi/asm/sockios.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/asm-generic/sockios.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/sockios.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/uio.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/uio.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/socket.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/if.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/libc-compat.h \
    $(wildcard include/config/data.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/hdlc/ioctl.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/aio_abi.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/compat.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/sched/task_stack.h \
    $(wildcard include/config/debug/stack/usage.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/magic.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/user32.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/ethtool.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/if_ether.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/skbuff.h \
    $(wildcard include/config/nf/conntrack.h) \
    $(wildcard include/config/bridge/netfilter.h) \
    $(wildcard include/config/ipv6/ndisc/nodetype.h) \
    $(wildcard include/config/net/rx/busy/poll.h) \
    $(wildcard include/config/network/secmark.h) \
    $(wildcard include/config/network/phy/timestamping.h) \
    $(wildcard include/config/netfilter/xt/target/trace.h) \
    $(wildcard include/config/nf/tables.h) \
    $(wildcard include/config/ip/vs.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/net.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/random.h \
    $(wildcard include/config/gcc/plugin/latent/entropy.h) \
    $(wildcard include/config/arch/random.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/once.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/random.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/archrandom.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/net.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/textsearch.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/net/checksum.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/uaccess.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/kasan-checks.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/uaccess.h \
    $(wildcard include/config/x86/intel/usercopy.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/smap.h \
    $(wildcard include/config/x86/smap.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/uaccess_64.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/checksum.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/checksum_64.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/netdev_features.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/sched/clock.h \
    $(wildcard include/config/have/unstable/sched/clock.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/net/flow_dissector.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/in6.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/in6.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/if_ether.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/splice.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/pipe_fs_i.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/if_packet.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/net/flow.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/net/net_namespace.h \
    $(wildcard include/config/ieee802154/6lowpan.h) \
    $(wildcard include/config/ip/sctp.h) \
    $(wildcard include/config/ip/dccp.h) \
    $(wildcard include/config/netfilter.h) \
    $(wildcard include/config/nf/defrag/ipv6.h) \
    $(wildcard include/config/netfilter/netlink/acct.h) \
    $(wildcard include/config/nf/ct/netlink/timeout.h) \
    $(wildcard include/config/wext/core.h) \
    $(wildcard include/config/mpls.h) \
    $(wildcard include/config/can.h) \
    $(wildcard include/config/net/ns.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/net/netns/core.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/net/netns/mib.h \
    $(wildcard include/config/xfrm/statistics.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/net/snmp.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/snmp.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/u64_stats_sync.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/net/netns/unix.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/net/netns/packet.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/net/netns/ipv4.h \
    $(wildcard include/config/ip/multiple/tables.h) \
    $(wildcard include/config/ip/route/classid.h) \
    $(wildcard include/config/ip/mroute.h) \
    $(wildcard include/config/ip/mroute/multiple/tables.h) \
    $(wildcard include/config/ip/route/multipath.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/net/inet_frag.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/net/netns/ipv6.h \
    $(wildcard include/config/ipv6/multiple/tables.h) \
    $(wildcard include/config/ipv6/mroute.h) \
    $(wildcard include/config/ipv6/mroute/multiple/tables.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/net/dst_ops.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/net/netns/ieee802154_6lowpan.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/net/netns/sctp.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/net/netns/dccp.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/net/netns/netfilter.h \
    $(wildcard include/config/nf/defrag/ipv4.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/netfilter_defs.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/netfilter.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/in.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/in.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/net/netns/x_tables.h \
    $(wildcard include/config/bridge/nf/ebtables.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/net/netns/conntrack.h \
    $(wildcard include/config/nf/ct/proto/dccp.h) \
    $(wildcard include/config/nf/ct/proto/sctp.h) \
    $(wildcard include/config/nf/conntrack/events.h) \
    $(wildcard include/config/nf/conntrack/labels.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/netfilter/nf_conntrack_tcp.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/netfilter/nf_conntrack_tcp.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/net/netns/nftables.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/net/netns/xfrm.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/xfrm.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/net/netns/mpls.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/net/netns/can.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/ns_common.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/seq_file_net.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/net/netprio_cgroup.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/cgroup.h \
    $(wildcard include/config/sock/cgroup/data.h) \
    $(wildcard include/config/cgroup/net/classid.h) \
    $(wildcard include/config/cgroup/data.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/cgroupstats.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/taskstats.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/nsproxy.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/user_namespace.h \
    $(wildcard include/config/inotify/user.h) \
    $(wildcard include/config/persistent/keyrings.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/cgroup-defs.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/bpf-cgroup.h \
    $(wildcard include/config/cgroup/bpf.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/bpf.h \
    $(wildcard include/config/efficient/unaligned/access.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/bpf_common.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/cgroup_subsys.h \
    $(wildcard include/config/cgroup/cpuacct.h) \
    $(wildcard include/config/sched/tune.h) \
    $(wildcard include/config/blk/cgroup.h) \
    $(wildcard include/config/cgroup/device.h) \
    $(wildcard include/config/cgroup/freezer.h) \
    $(wildcard include/config/cgroup/perf.h) \
    $(wildcard include/config/cgroup/hugetlb.h) \
    $(wildcard include/config/cgroup/pids.h) \
    $(wildcard include/config/cgroup/rdma.h) \
    $(wildcard include/config/cgroup/debug.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/neighbour.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/netlink.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/net/scm.h \
    $(wildcard include/config/security/network.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/security.h \
    $(wildcard include/config/security/infiniband.h) \
    $(wildcard include/config/security/network/xfrm.h) \
    $(wildcard include/config/security/path.h) \
    $(wildcard include/config/audit.h) \
    $(wildcard include/config/securityfs.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/netlink.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/netdevice.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/if_link.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/if_link.h \
    $(wildcard include/config/pending.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/if_bonding.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/pkt_cls.h \
    $(wildcard include/config/net/cls/ind.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/pkt_sched.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/hashtable.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/etherdevice.h \
    $(wildcard include/config/have/efficient/unaligned/access.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/unaligned.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/unaligned/access_ok.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/unaligned/generic.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/mii.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/mii.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/crc32.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/bitrev.h \
    $(wildcard include/config/have/arch/bitreverse.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/ip.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/ip.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/ipv6.h \
    $(wildcard include/config/ipv6/router/pref.h) \
    $(wildcard include/config/ipv6/route/info.h) \
    $(wildcard include/config/ipv6/optimistic/dad.h) \
    $(wildcard include/config/ipv6/seg6/hmac.h) \
    $(wildcard include/config/ipv6/mip6.h) \
    $(wildcard include/config/ipv6/subtrees.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/ipv6.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/icmpv6.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/icmpv6.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/tcp.h \
    $(wildcard include/config/tcp/md5sig.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/win_minmax.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/net/sock.h \
    $(wildcard include/config/inet.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/page_counter.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/memcontrol.h \
    $(wildcard include/config/memcg/swap.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/vmpressure.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/eventfd.h \
    $(wildcard include/config/eventfd.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/writeback.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/flex_proportions.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/backing-dev-defs.h \
    $(wildcard include/config/debug/fs.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/blk_types.h \
    $(wildcard include/config/blk/dev/throttling/low.h) \
    $(wildcard include/config/blk/dev/integrity.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/bvec.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/filter.h \
    $(wildcard include/config/arch/has/set/memory.h) \
    $(wildcard include/config/bpf/jit.h) \
    $(wildcard include/config/have/ebpf/jit.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/cryptohash.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/set_memory.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/asm/set_memory.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/asm-generic/set_memory.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/net/sch_generic.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/net/gen_stats.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/gen_stats.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/rtnetlink.h \
    $(wildcard include/config/net/ingress.h) \
    $(wildcard include/config/net/egress.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/rtnetlink.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/if_addr.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/net/rtnetlink.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/net/netlink.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/filter.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/rculist_nulls.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/poll.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/poll.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/arch/x86/include/uapi/asm/poll.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/asm-generic/poll.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/eventpoll.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/net/dst.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/net/neighbour.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/net/tcp_states.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/net_tstamp.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/net/smc.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/net/inet_connection_sock.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/net/inet_sock.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/net/request_sock.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/net/netns/hash.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/net/l3mdev.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/net/fib_rules.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/fib_rules.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/net/fib_notifier.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/net/inet_timewait_sock.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/net/timewait_sock.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/tcp.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/udp.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/udp.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/if_vlan.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/if_vlan.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/net/ip6_checksum.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/net/ip.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/net/route.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/net/inetpeer.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/net/ipv6.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/net/if_inet6.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/net/ndisc.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/linux/if_arp.h \
    $(wildcard include/config/firewire/net.h) \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/if_arp.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/net/ip_fib.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/in_route.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/include/uapi/linux/route.h \
  /var/tmp/portage/sys-kernel/chromeos-kernel-4_14-4.14.123-r807/work/chromeos-kernel-4_14-4.14.123/drivers/net/ethernet/jme.h \

drivers/net/ethernet/jme.o: $(deps_drivers/net/ethernet/jme.o)

$(deps_drivers/net/ethernet/jme.o):
