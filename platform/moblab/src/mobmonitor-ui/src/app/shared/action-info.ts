export interface ActionInfo {
  action: string;
  info: string;
  params: {[key: string]: string};
}
