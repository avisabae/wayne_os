# Copyright (c) 2012 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.
from fuzzy_check import FuzzyCheck
from validators import *

def Validate(raw, events, gestures):
  """
    This test checks if fling stop events are sent before button events, as this
    is required by Chrome.
  """
  fuzzy = FuzzyCheck()
  fuzzy.expected = [
    MotionValidator("> 10"),
    FlingStopValidator(),
    ButtonDownValidator(1),
    ButtonUpValidator(1),
  ]
  fuzzy.unexpected = [
    MotionValidator("<10"),
    FlingStopValidator("<10")
  ]
  return fuzzy.Check(gestures)
