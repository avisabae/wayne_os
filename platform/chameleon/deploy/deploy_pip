#!/bin/sh
# Copyright 2016 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

SCRIPT="$(readlink -f $0)"
UPDATABLE_DIR="$(realpath $(dirname ${SCRIPT})/../updatable)"
SRC_PYTHON_DIR="${UPDATABLE_DIR}/python2.7"
PIP_DIR="${UPDATABLE_DIR}/pip"
DST_PYTHON_DIR="/usr/lib/python2.7"
PIP_BOOTSTRAP="get-pip.py"

set_time() {
    echo Current chameleon time: $(date --set="$NOW")
}

install_standard_python_modules() {
    for file in "${SRC_PYTHON_DIR}"/*; do
        cp -f "${file}" "${DST_PYTHON_DIR}"
    done
}

install_pip() {
    python "${PIP_DIR}/${PIP_BOOTSTRAP}" --no-setuptools
}

install_setuptools() {
    pip install setuptools==24.0.2
}

uninstall_old_chameleond_from_pip() {
    # On the first installation of PIP, Chameleond was already there,
    # i.e /usr/lib/python2.7/site-packages/chameleond.
    # Since then, the new Chameleond is installed in a different path,
    # i.e. /usr/lib/python2.7/site-packages/chameleond-0.0.2-py2.7.egg.
    # So check the package directory and uninstall the old Chameleond.
    pip show chameleond | grep 'Location: .*\.egg' > /dev/null
    if [ $? -ne 0 ]; then
        echo PIP still has the old chameleond. Uninstall it...
        pip uninstall -y chameleond
        rm -rf /usr/lib/python2.7/site-packages/chameleond
    fi
}

# Set the correct time. Otherwise, installation of pip may fail.
set_time

pip -V
if [ $? -eq 0 ]; then
    echo PIP is already installed. Skip installing it again.
    exit 0
fi

# Install the missing standard python modules needed to install pip.
install_standard_python_modules
install_pip
install_setuptools

# Uninstall old chameleond package from pip
uninstall_old_chameleond_from_pip
