#!/bin/sh
# Copyright (c) 2014 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

### BEGIN INIT INFO
# Provides:          chameleon-updater
# Required-Start:    $network $remote_fs
# Required-Stop:     $network $remote_fs
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Start chameleon-updater at boot time
# Description:       Auto-update chameleond by checking any new version in
#                    the Google Cloud Storage and installing it.
### END INIT INFO

DAEMON_NAME="chameleon_updater"
CONFIG_FILE="/etc/default/chameleond"
. "${CONFIG_FILE}"

DAEMON="${CHAMELEOND_DIR}/run_chameleon_updater"
DAEMON_USER='root'
PIDFILE="/var/run/${DAEMON_NAME}.pid"
LOGFILE="/var/log/${DAEMON_NAME}.log"

do_start () {
    echo "Check new chameleon bundle"
    # Start a daemon, so it won't be killed when reboot.
    start-stop-daemon --start --background --pidfile "${PIDFILE}" \
        --make-pidfile --user "${DAEMON_USER}" --chuid "${DAEMON_USER}" \
        --startas /bin/bash -- \
        -c "exec ${DAEMON} start >> ${LOGFILE} 2>&1"
}

do_stop () {
    # Call the daemon stop directly, it will check if the updating process is
    # running or not and wait until the updating process is done.
    ${DAEMON} stop
    rm -f "${PIDFILE}"
}

case "$1" in
    start|stop)
        do_${1}
        ;;

    restart)
        do_stop
        do_start
        ;;

    *)
        echo "Usage: /etc/init.d/${UPDATER_NAME} {start|stop|restart}"
        exit 1
        ;;
esac
exit 0
