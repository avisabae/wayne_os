#!/bin/sh
#
# Copyright 2019 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

. /usr/share/misc/shflags
. /opt/google/touch/scripts/chromeos-touch-common.sh
. /opt/google/touch/scripts/chromeos-etphidiap-hw-track.sh

DEFINE_string 'device' '' "i2c-dev device name e.g. i2c-7" 'd'
DEFINE_string 'device_path' '' "device path in /sys" 'p'
DEFINE_boolean 'recovery' ${FLAGS_FALSE} "Recovery. Allows for rollback" 'r'

# Parse command line.
FLAGS "$@" || exit 1
eval set -- "${FLAGS_ARGV}"

# Actually trigger a firmware update by running the ELAN update tool in minijail
# to limit the syscalls it can access.
update_firmware() {
  local fw_link=
  local cmd_log=
  local ret=

  fw_link="$1" || return; shift

  cmd_log="$(
    minijail0 -u fwupdate-i2c -g fwupdate-i2c \
      -G -I -N -n -p -v --uts \
      -S /opt/google/touch/policies/etphidiap.update.policy -- \
      /usr/sbin/etphid_updater -i "${FLAGS_device#i2c-}" -b "${fw_link}" 2>&1
  )"

  ret="$?"
  if [ "${ret}" -ne 0 ]; then
    die "Exit status ${ret} updating touchpad firmware: ${cmd_log}"
  fi
  return "${ret}"
}

# The on-disk FW version is determined by reading the filename which
# is in the format "etphidiap_firmware_0xVER.bin".  We follow the fw's link
# to the actual file then strip away everything in the FW's filename
# but the 0xVER version (keeping 0x prefix).
#
# TODO(https://issuetracker.google.com/79157488): Include hardware ID in the
# firmware filename once it is available.  See
# https://issuetracker.google.com/129152245 for more background.
get_fw_version_from_disk() {
  local fw_link=
  local fw_filepath=
  local fw_filename=
  local fw_ver=

  fw_link="$1" || return; shift

  [ -L "${fw_link}" ] || return
  fw_filepath="$(realpath -e "${fw_link}")" || return
  [ -n "${fw_filepath}" ] || return

  fw_filename="$(basename "${fw_filepath}")"
  fw_ver="${fw_filename#*_}"
  fw_ver="${fw_ver%.*}"
  printf '0x%X\n' "${fw_ver}"
}

get_active_fw_version() {
  # Query the touchpad and see what the current FW version it's running.
  local active_fw_ver=
  local ret=

  active_fw_ver="$(
    minijail0 -u fwupdate-i2c -g fwupdate-i2c \
      -G -I -N -n -p -v --uts \
      -S /opt/google/touch/policies/etphidiap.query.policy -- \
      /usr/sbin/etphid_updater -i "${FLAGS_device#i2c-}" -g 2>&1
  )"

  ret="$?"
  if [ "${ret}" -eq 0 ]; then
    # The printing of "-1" to stdout is hardcoded in etphid_updater.  Unlike
    # actual version numbers, "-1" does not represent a hexadecimal number.
    if [ "${active_fw_ver}" != "-1" ]; then
      printf '0x%X\n' "0x${active_fw_ver}"
    else
      echo 1>&2 "Error on stdout retrieving touchpad firmware version: "\
"${active_fw_ver}"
    fi
  else
    echo 1>&2 "Exit status ${ret} retrieving touchpad firmware version: "\
"${active_fw_ver}"
  fi
  return "${ret}"
}

main() {
  local active_fw_ver=
  local new_fw_ver=
  local update_type=
  local update_needed=
  local fw_link=
  local active_fw_hw_rev=
  local new_fw_hw_rev=
  local chassis_id=
  local board_rev=

  # This script runs early at bootup, so if the touch driver is mistakenly
  # included as a module (as opposed to being compiled directly in) the i2c
  # device may not be present yet. Pause long enough for for people to notice
  # and fix the kernel config.
  check_i2c_chardev_driver

  chassis_id="$(get_chassis_id)"
  board_rev="$(get_platform_ver)"

  log_msg "Chassis identifier detected as: ${chassis_id}"
  log_msg "Platform version detected as: ${board_rev}"

  fw_link="$(find_fw_link_path "etphidiap_firmware.bin" "${chassis_id}")"
  log_msg "Attempting to Load FW: ${fw_link}"

  active_fw_ver="$(get_active_fw_version)"
  new_fw_ver="$(get_fw_version_from_disk "${fw_link}")"
  log_msg "Active firmware version: ${active_fw_ver}"
  log_msg "New firmware version: ${new_fw_ver}"
  [ -n "${active_fw_ver}" ] || die "Unable to determine active FW version."
  [ -n "${new_fw_ver}" ] || die "Unable to find new FW version on disk."

  if [ "$(( ${active_fw_ver} == 0xFFFFFFFF ))" -gt 0 ]; then
    log_msg "Active firmware appears to be corrupt, will update firmware."
    update_needed="${FLAGS_TRUE}"

  else
    # TODO(https://issuetracker.google.com/79157488): Replace get_tp_hw_track
    # with using actual hardware ID once that becomes available.  See
    # https://issuetracker.google.com/129152245 for more background.
    type get_tp_hw_track | head -n 1 | grep -q -E ' ?function$' || die \
"No get_tp_hw_track() function is defined, this is a bug in the board overlay."
    active_fw_hw_rev="$(get_tp_hw_track ${chassis_id} ${active_fw_ver})"
    new_fw_hw_rev="$(get_tp_hw_track ${chassis_id} ${new_fw_ver})"
    log_msg "Hardware revision for active firmware version: ${active_fw_hw_rev}"
    log_msg "Hardware revision for new firmware version: ${new_fw_hw_rev}"
    [ -n "${new_fw_hw_rev}" ] || die "Unable to look up HW rev for new FW."

    if [ -z "${active_fw_hw_rev}" ]; then
      log_msg "The HW revision for active touchpad FW version "\
"${active_fw_ver} is unknown.  Will not attempt FW update."
      exit 0
    fi

    if [ "${active_fw_hw_rev}" != "${new_fw_hw_rev}" ]; then
      log_msg "HW revision "${new_fw_hw_rev}" for on disk touchpad FW does "\
"not match hardware revision "${active_fw_hw_rev}" for active touchpad FW.  "\
"Will not attempt FW update."
      exit 0
    fi

    update_type="$(compare_multipart_version "${active_fw_ver}" \
      "${new_fw_ver}")"
    log_update_type "${update_type}"
    update_needed="$(is_update_needed "${update_type}")"
  fi

  if [ "${update_needed}" -eq "${FLAGS_TRUE}" ]; then
    log_msg "Updating FW to ${new_fw_ver}"
    chromeos-boot-alert update_touchpad_firmware
    run_cmd_and_block_powerd update_firmware "${fw_link}"

    # Check if update was successful
    active_fw_ver="$(get_active_fw_version)"
    update_type="$(compare_multipart_version "${active_fw_ver}" \
      "${new_fw_ver}")"

    if [ "${update_type}" -ne "${UPDATE_NOT_NEEDED_UP_TO_DATE}" ]; then
      die "Firmware update failed. Current Firmware: ${active_fw_ver}"
    fi
    log_msg "Update FW succeded. Current Firmware: ${active_fw_ver}"

    rebind_driver "${FLAGS_device_path}"
  fi

  exit 0
}

main "$@"
