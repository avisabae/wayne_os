OS Chrome nedostaje ili je oštećen.
Umetnite USB memorijski uređaj ili SD karticu za oporavak.
Umetnite USB memorijski uređaj za oporavak.
Umetnite SD karticu ili USB memorijski uređaj za oporavak (napomena: plavi USB priključak NE funkcionira za oporavak).
Umetnite USB memorijski uređaj za oporavak u jedan od četiri priključka sa STRAŽNJE strane uređaja.
Uređaj koji ste umetnuli ne sadrži OS Chrome.
Potvrda OS-a ISKLJUČENA je
Pritisnite RAZMAKNICU kako biste je ponovo omogućili.
Pritisnite tipku ENTER kako biste potvrdili da želite uključiti potvrdu OS-a.
Sustav će se ponovo pokrenuti i lokalni će podaci biti obrisani.
Za povratak pritisnite ESC.
Potvrda OS-a UKLJUČENA je.
Da biste ISKLJUČILI potvrdu OS-a, pritisnite tipku ENTER.
Pomoć potražite na https://google.com/chromeos/recovery
Kôd pogreške
Uklonite sve vanjske uređaje za početak oporavka.
Model 60061e
Da biste isključili potvrđivanje OS-a, pritisnite gumb za oporavak.
Priključeni izvor napajanja nema dovoljno snage za napajanje ovog uređaja.
OS Chrome sada će se isključiti.
Priključite odgovarajući adapter i pokušajte ponovno.
Uklonite sve povezane uređaje i pokrenite oporavak.
Pritisnite brojčanu tipku da biste odabrali alternativni početni program za pokretanje:
Pritisnite tipku za UKLJUČIVANJE/ISKLJUČIVANJE da biste pokrenuli dijagnostiku.
Da biste ISKLJUČILI potvrdu OS-a, pritisnite tipku za UKLJUČIVANJE/ISKLJUČIVANJE.
