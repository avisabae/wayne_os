Chrome OS est manquant ou endommagé.
Veuillez insérer une clé USB ou une carte SD de restauration.
Veuillez insérer une clé USB de restauration.
Veuillez insérer une clé USB ou une carte SD de restauration. Remarque : le port USB bleu ne fonctionne PAS pour la restauration.
Veuillez insérer une clé USB de restauration dans l'un des quatre ports USB à l'ARRIÈRE de cet appareil.
Le périphérique que vous avez inséré ne contient pas Chrome OS.
La validation du système d'exploitation est DÉSACTIVÉE
Appuyez sur ESPACE pour la réactiver.
Appuyez sur ENTRÉE pour confirmer que vous souhaitez activer la validation du système d'exploitation.
Votre système va redémarrer et les données locales vont être effacées.
Pour annuler, appuyez sur ÉCHAP.
La validation du système d'exploitation est ACTIVÉE.
Pour DÉSACTIVER la validation du système d'exploitation, appuyez sur ENTRÉE.
Pour obtenir de l'aide, rendez-vous sur https://google.com/chromeos/recovery
Code d'erreur
Veuillez débrancher tous les périphériques externes pour lancer la restauration.
Modèle 60061e
Pour DÉSACTIVER la validation du système d'exploitation, appuyez sur le bouton RESTAURATION.
L'alimentation sur laquelle l'appareil est branché n'est pas assez puissante.
Chrome OS va se fermer.
Veuillez utiliser le bon adaptateur secteur, puis réessayer.
Veuillez débrancher tous les appareils connectés et lancer la restauration.
Appuyez sur une touche numérique pour sélectionner un autre bootloader:
Appuyez sur le bouton Marche/Arrêt pour exécuter des diagnostics.
Pour désactiver la validation de l'OS, appuyez sur le bouton Marche/Arrêt.
