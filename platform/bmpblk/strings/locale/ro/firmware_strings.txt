Sistemul de operare Chrome lipsește sau este deteriorat.
Introduceți un stick USB sau un card SD de recuperare.
Introduceți un stick USB de recuperare.
Introduceți un card SD sau un stick USB de recuperare (notă: portul USB albastru NU va funcționa pentru recuperare).
Introduceți un stick USB de recuperare în unul dintre cele 4 porturi din SPATELE dispozitivului.
Dispozitivul introdus de dvs. nu conține Sistemul de operare Chrome.
Verificarea sistemului de operare este DEZACTIVATĂ
Apăsați pe tasta SPACE pentru a o reactiva.
Apăsați pe ENTER pentru a confirma că doriți să activați verificarea sistemului de operare.
Sistemul va reporni și datele locale vor fi șterse.
Pentru a reveni, apăsați pe ESC.
Verificarea sistemului de operare este ACTIVATĂ.
Pentru a DEZACTIVA verificarea sistemului de operare, apăsați pe ENTER.
Pentru asistență, accesați https://google.com/chromeos/recovery
Cod de eroare
Eliminați toate dispozitivele externe pentru a începe recuperarea.
Model 60061e
Pentru a DEZACTIVA verificarea sistemului de operare, apăsați pe butonul RECUPERARE.
Adaptorul de curent conectat nu are suficientă putere pentru ca dispozitivul să funcționeze.
Acum sistemul de operare Chrome se va închide.
Folosiți adaptorul potrivit și încercați din nou.
Eliminați toate dispozitivele conectate și începeți recuperarea.
Apăsați o tastă numerică pentru a selecta un bootloader alternativ:
Apăsați butonul de pornire pentru a rula diagnosticarea.
Pentru a DEZACTIVA verificarea sistemului de operare, apăsați butonul de PORNIRE.
