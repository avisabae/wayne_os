OS Chrome tiada atau rosak.
Sila masukkan batang USB atau kad SD pemulihan.
Sila masukkan batang USB pemulihan.
Sila masukkan batang USB atau kad SD pemulihan (perhatian: port USB biru TIDAK akan berfungsi untuk pemulihan).
Sila masukkan batang USB pemulihan ke dalam salah satu daripada 4 port di BELAKANG peranti.
Peranti yang anda masukkan tidak mengandungi OS Chrome.
Pengesahan OS DIMATIKAN
Tekan RUANG untuk mendayakan semula.
Tekan ENTER untuk mengesahkan yang anda ingin menghidupkan pengesahan OS.
Sistem anda akan but semula dan data setempat akan dihapuskan.
Untuk kembali, tekan ESC.
Pengesahan OS DIHIDUPKAN.
Untuk MEMATIKAN pengesahan OS, tekan ENTER.
Untuk mendapatkan bantuan, lawati https://google.com/chromeos/recovery
Kod ralat
Sila alih keluar semua peranti luaran untuk memulakan pemulihan.
Model 60061e
Untuk MEMATIKAN pengesahan OS, tekan butang PEMULIHAN.
Bekalan kuasa yang disambungkan tiada kuasa yang mencukupi untuk menjalankan peranti ini.
OS Chrome akan ditutup sekarang.
Sila gunakan penyesuai yang betul dan cuba lagi.
Sila alih keluar semua peranti yang disambungkan dan mulakan pemulihan.
Tekan kekunci nombor untuk memilih pemuat but alternatif:
Tekan butang KUASA untuk menjalankan diagnostik.
Untuk MEMATIKAN pengesahan OS, tekan butang KUASA.
