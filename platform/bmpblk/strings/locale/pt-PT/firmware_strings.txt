O SO Chrome está em falta ou danificado.
Insira um cartão SD ou memory stick USB de recuperação.
Insira um memory stick USB de recuperação.
Insira um cartão SD ou memory stick USB de recuperação (nota: a porta USB azul NÃO funciona para recuperação).
Insira um memory stick USB de recuperação numa das 4 portas na PARTE POSTERIOR do dispositivo.
O dispositivo que inseriu não contém o SO Chrome.
A Validação do SO está DESATIVADA
Prima ESPAÇO para reativá-la.
Prima ENTER para confirmar que pretende ativar a Validação do SO.
O sistema reinicia e os dados locais são apagados.
Para retroceder, prima ESC.
A Validação do SO está ATIVADA.
Para definir a Validação do SO como DESATIVADA, prima ENTER.
Para obter ajuda, visite https://google.com/chromeos/recovery
Código do erro
Remova todos os dispositivos externos para iniciar a recuperação.
Modelo 60061e
Para desativar a Validação do SO, prima o botão RECOVERY.
A fonte de alimentação ligada não tem energia suficiente para alimentar este dispositivo.
O SO Chrome será agora encerrado.
Utilize o carregador correto e tente novamente.
Remova todos os dispositivos ligados e inicie a recuperação.
Prima uma tecla numérica para selecionar um carregador de arranque alternativo:
Prima o botão LIGAR/DESLIGAR para executar os diagnósticos.
Para desativar a Validação do SO, prima o botão LIGAR/DESLIGAR.
